package com.example.directoriov2.Modelos;

public class PuntosP {

    private String tipo;
    private String nivel;
    private String fecha;
    private String down;
    private String descripcion;
    private String valorcom;
    private String porcentaje;
    private String comision;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDown() {
        return down;
    }

    public void setDown(String down) {
        this.down = down;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValorcom() {
        return valorcom;
    }

    public void setValorcom(String valorcom) {
        this.valorcom = valorcom;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getComision() {
        return comision;
    }

    public void setComision(String comision) {
        this.comision = comision;
    }

    public PuntosP(String tipo, String nivel, String fecha, String down, String descripcion, String valorcom, String porcentaje, String comision) {
        this.tipo = tipo;
        this.nivel = nivel;
        this.fecha = fecha;
        this.down = down;
        this.descripcion = descripcion;
        this.valorcom = valorcom;
        this.porcentaje = porcentaje;
        this.comision = comision;
    }
}
