package com.example.directoriov2.Fragments.Listas;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.directoriov2.Adaptadores.AdaptadorListaProductos;
import com.example.directoriov2.DescripcionActivity;
import com.example.directoriov2.MainActivity;
import com.example.directoriov2.Modelos.Productos;
import com.example.directoriov2.R;
import com.example.directoriov2.serviciosAPI.APIServices;
import com.example.directoriov2.serviciosAPI.InstanciaRetrofit;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListaProductosFragment extends Fragment {
    View view;
    ArrayList<Productos> listaProductos;
    RecyclerView recyclerView;
    AdaptadorListaProductos adaptador;
    private OnFragmentInteractionListener mListener;

    public ListaProductosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lista_productos, container, false);
        init();
        return view;
    }

    private void init() {
        listaProductos = new ArrayList<>();
        cargarRecycler();
    }


    private void cargarRecycler() {
        RecyclerView.LayoutManager layoutManager;
        recyclerView = view.findViewById(R.id.RV_lista_productos);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        respuestaAPI(); //respuesta del API

        adaptador = new AdaptadorListaProductos(listaProductos, getActivity(), new AdaptadorListaProductos.OnItemClickListener() {
            @Override
            public void onItemClick(Productos item) {
                Intent intent = new Intent(getActivity(), DescripcionActivity.class);
                intent.putExtra("objeto", (Serializable) item);
                intent.putExtra("tag",DescripcionActivity.TAG_LISTA_PRODUCTOS);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adaptador);
    }

    private void respuestaAPI() {
        APIServices api = InstanciaRetrofit.getRetrofitInstance().create(APIServices.class);
        Call<ArrayList<Productos>> call = api.getProductos();
        call.enqueue(new Callback<ArrayList<Productos>>() {
            @Override
            public void onResponse(Call<ArrayList<Productos>> call, Response<ArrayList<Productos>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Productos> productos = response.body();
                    for (Productos p : productos) {
                        llenarProductos(p);
                    }
                    adaptador.notifyDataSetChanged();
                } else {
                    Toast.makeText(getActivity(), "error en los datos ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Productos>> call, Throwable t) {
                Toast.makeText(getActivity(), "hubo un error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void llenarProductos(Productos productos) {
        String descripcion;
        // descripcion = "http://" + productos.getSubdominio() + ".yatvii.xyz/";
        //descripcion = productos.getSubdominio();
        String url_imagen = productos.getUrl() + formatoImagenSmall(productos.getImage_url());
        productos.setImage_url(url_imagen);
        //productos.setDescripcion(descripcion);
        listaProductos.add(productos);
    }

    public String formatoImagenTiny(String imagen) {
        return imagen.replace("/original/", "/tiny/");
    }

    public String formatoImagenSmall(String imagen) {
        return imagen.replace("/original/", "/small/");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        final SearchView buscador;
        inflater.inflate(R.menu.menu_search, menu);
        buscador = (SearchView) menu.findItem(R.id.buscador).getActionView();
        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adaptador.getFilter().filter(newText);
                return true;
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.carrito) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra("opcion", 1);
            startActivity(intent);

//            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}