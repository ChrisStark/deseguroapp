package com.example.directoriov2.Modelos;

import android.widget.Button;

public class Paquetes {

    private String nombrepaq;
    private String puntospaq;
    private String serviciospaq;
    private String numservpaq;
    private String preciopaq;
    private Button btn_pagarefectivo;
    private String pagarptos;

    public String getNombrepaq() {
        return nombrepaq;
    }

    public void setNombrepaq(String nombrepaq) {
        this.nombrepaq = nombrepaq;
    }

    public String getPuntospaq() {
        return puntospaq;
    }

    public void setPuntospaq(String puntospaq) {
        this.puntospaq = puntospaq;
    }

    public String getServiciospaq() {
        return serviciospaq;
    }

    public void setServiciospaq(String serviciospaq) {
        this.serviciospaq = serviciospaq;
    }

    public String getNumservpaq() {
        return numservpaq;
    }

    public void setNumservpaq(String numservpaq) {
        this.numservpaq = numservpaq;
    }

    public String getPreciopaq() {
        return preciopaq;
    }

    public void setPreciopaq(String preciopaq) {
        this.preciopaq = preciopaq;
    }

    public Paquetes(String nombrepaq, String puntospaq, String serviciospaq, String numservpaq, String preciopaq) {

        this.nombrepaq = nombrepaq;
        this.puntospaq = puntospaq;
        this.serviciospaq = serviciospaq;
        this.numservpaq = numservpaq;
        this.preciopaq = preciopaq;

    }
}
