package com.example.directoriov2.Clases;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.os.Bundle;
import android.os.IBinder;

import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Ubicacion extends Service implements LocationListener {

    private final Context context;
    private double latitud;
    private double longitud;
    private Location location;
    private String ciudad;
    LocationManager locationManager;
    boolean gpsActivo;

    public Ubicacion() {
        super();
        this.context = this.getApplicationContext();
    }

    public Ubicacion(Context c) {
        super();
        this.context = c;
        //  getLocation();

    }

    public void mostrarUbicacion(){
        Toast.makeText(context, "Ciudad: "+ciudad, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("MissingPermission")
    public void getLocation() {
        try {
            locationManager = (LocationManager) this.context.getSystemService(LOCATION_SERVICE);
            gpsActivo = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (gpsActivo) {
                locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER
                        , (0)
                        , 0
                        , this);

                location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
                getCiudad(location);
                latitud = location.getLatitude();
                longitud = location.getLongitude();
                mostrarUbicacion();
            }
            else {
                Toast.makeText(context, "gps desactivado", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) { }

    }

    public void getCiudad(Location location) {
        if (location.getLatitude() != 0.0 && location.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        location.getLatitude(), location.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address dirCalle = list.get(0);
                    ciudad =  dirCalle.getAdminArea()+" "+dirCalle.getLocality();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        //   Toast.makeText(context, "GPS activado", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        // Toast.makeText(context, "GPS desactivado", Toast.LENGTH_SHORT).show();
    }
}
