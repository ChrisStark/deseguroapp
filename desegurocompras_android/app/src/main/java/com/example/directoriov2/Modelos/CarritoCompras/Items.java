package com.example.directoriov2.Modelos.CarritoCompras;

public class Items {
    private Item item;
    private Publicacion publicacion;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }
}
