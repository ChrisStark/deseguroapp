package com.example.directoriov2.Modelos;

import java.io.Serializable;

public class Negocio implements Serializable {

    private String titulo;
    private String imagen;
    private String descrip;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Negocio(String titulo, String imagen, String descrip) {
        this.titulo = titulo;
        this.imagen = imagen;
        this.descrip = descrip;
    }
}
