package com.example.directoriov2.Clases;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.directoriov2.Adaptadores.AdaptadorDialogoPreguntas;
import com.example.directoriov2.Modelos.CarritoCompras.Carrito;
import com.example.directoriov2.Modelos.ModelDialogoPreguntas;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class DialogoPreguntas extends DialogFragment implements View.OnClickListener {
    public static String TAG = "ModelDialogoPreguntas";
    View view;
    ArrayList<ModelDialogoPreguntas> listaPreguntas;
    AdaptadorDialogoPreguntas adaptador;
    RecyclerView recycler;
    ImageView preguntar;
    EditText nueva_pregunta;

    public DialogoPreguntas() {
    }

    public static DialogoConcretarCompra newInstance(Carrito carrito) {
        DialogoConcretarCompra frag = new DialogoConcretarCompra();
        Bundle args = new Bundle();
        args.putSerializable("objeto", carrito);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.EstiloFullScreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogo_preguntas, container, false);
        setToolbar();
        init();
        return view;
    }

    public void init() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        nueva_pregunta = view.findViewById(R.id.nueva_pregunta);
        preguntar = view.findViewById(R.id.btnPreguntar);
        listaPreguntas = new ArrayList<>();
        recycler = view.findViewById(R.id.RVdialogo_preguntas);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        llenarProductos();
        adaptador = new AdaptadorDialogoPreguntas(getContext(), listaPreguntas);
        adaptador.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);

                int msgCount = adaptador.getItemCount();
                int lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();

                if (lastVisiblePosition == -1 ||
                        (positionStart >= (msgCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    recycler.scrollToPosition(positionStart);
                }
            }
        });
        recycler.setAdapter(adaptador);
        preguntar.setOnClickListener(this);
    }

    private void llenarProductos() {
        listaPreguntas.add(new ModelDialogoPreguntas("cuanto tiempo me tardaria e llegar el producto que solicite y tambien si tiene en color negro", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("tengo una duda", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("cuanto tiempo me tardaria e llegar el producto que solicite y tambien si tiene en color negro", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("tengo una duda", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("cuanto tiempo me tardaria e llegar el producto que solicite y tambien si tiene en color negro", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("tengo una duda", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("cuanto tiempo me tardaria e llegar el producto que solicite y tambien si tiene en color negro", null, "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("tengo una duda", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("cuanto tiempo me tardaria e llegar el producto que solicite y tambien si tiene en color negro", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("tengo una duda", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("cuanto tiempo me tardaria e llegar el producto que solicite y tambien si tiene en color negro", "claro", "hace un mes"));
        listaPreguntas.add(new ModelDialogoPreguntas("tengo una duda", "claro", "hace un mes"));

    }

    private void setToolbar() {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setTitle("Preguntas");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                //  Toast.makeText(getActivity(), "clcik", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    public void addPregunta(ModelDialogoPreguntas item, int position) {
        listaPreguntas.add(position, item);
        adaptador.notifyItemInserted(position);
    }

    @Override
    public void onClick(View v) {
        if (nueva_pregunta.getText().toString().trim().isEmpty()) {

        } else {
            String texto = nueva_pregunta.getText().toString();
            ModelDialogoPreguntas item = new ModelDialogoPreguntas(texto, null, null);
            addPregunta(item, 0);
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(nueva_pregunta.getWindowToken(), 0);
            nueva_pregunta.setText("");


        }
    }

}
