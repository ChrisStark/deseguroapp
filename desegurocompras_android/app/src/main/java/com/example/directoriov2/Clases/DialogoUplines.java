package com.example.directoriov2.Clases;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.directoriov2.R;

public class DialogoUplines extends DialogFragment {

    public static final String TAG = "DialogoUplines";

    public DialogoUplines() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return createSimpleDialog();
    }


    //  String descneg, dueño, nombreneg, giro, nivmem;

    public AlertDialog createSimpleDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialogo_uplines, null);
        TextView dueno = v.findViewById(R.id.up_dueño);
        TextView nombreneg = v.findViewById(R.id.up_nombreneg);
        TextView giro = v.findViewById(R.id.up_giro);
        TextView nivel = v.findViewById(R.id.up_nivel);
        Button btn_verneg = v.findViewById(R.id.btn_verneg);

        builder.setView(v);
        builder.setNegativeButton("CANCELAR",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return builder.create();

    }

}