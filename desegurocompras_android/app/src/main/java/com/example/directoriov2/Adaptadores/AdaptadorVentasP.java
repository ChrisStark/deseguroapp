package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.directoriov2.Modelos.VentasP;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class AdaptadorVentasP extends RecyclerView.Adapter<AdaptadorVentasP.ViewHolderVentasP> {

    private Context context;
    private ArrayList<VentasP> ventasp;

    public AdaptadorVentasP (Context context, ArrayList<VentasP> ventasp){

        this.context =  context;
        this.ventasp =  ventasp;

    }


    @Override
    public AdaptadorVentasP.ViewHolderVentasP onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = 0;
        layout =  R.layout.ventas_pend_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderVentasP(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorVentasP.ViewHolderVentasP holder, int position) {

        holder.vpnombren.setText(ventasp.get(position).getVpnombren());
        holder.vpfecha.setText(ventasp.get(position).getVpfecha());
        holder.vpncomprador.setText(ventasp.get(position).getVpncomprador());
        holder.vpdescart.setText(ventasp.get(position).getVpdescart());
        holder.vpcantart.setText(ventasp.get(position).getVpcantart());
        holder.vpprecioart.setText(ventasp.get(position).getVpprecioart());
        holder.vptotalv.setText(ventasp.get(position).getVptotalv());

    }

    @Override
    public int getItemCount() {

        return ventasp.size();

    }

    public class ViewHolderVentasP extends RecyclerView.ViewHolder {

        private TextView vpnombren;
        private TextView vpfecha;
        private TextView vpncomprador;
        private TextView vpdescart;
        private TextView vpcantart;
        private TextView vpprecioart;
        private TextView vptotalv;

        public ViewHolderVentasP(View itemView) {

            super(itemView);
            vpnombren = itemView.findViewById(R.id.vp_nombren);
            vpfecha = itemView.findViewById(R.id.vp_fecha);
            vpncomprador = itemView.findViewById(R.id.vp_comprador);
            vpdescart =  itemView.findViewById(R.id.vp_nomart);
            vpcantart = itemView.findViewById(R.id.vp_cantart);
            vpprecioart = itemView.findViewById(R.id.vp_precioart);
            vptotalv = itemView.findViewById(R.id.vp_totalv);

        }
    }

}
