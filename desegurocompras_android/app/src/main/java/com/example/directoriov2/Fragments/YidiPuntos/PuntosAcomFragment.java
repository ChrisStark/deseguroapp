package com.example.directoriov2.Fragments.YidiPuntos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.AdaptadorPuntosA;
import com.example.directoriov2.Modelos.PuntosA;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class PuntosAcomFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<PuntosA> lista_puntosa;
    RecyclerView recyclerView;
    AdaptadorPuntosA adaptadorPuntosA;

    View view;

    private OnFragmentInteractionListener mListener;

    public PuntosAcomFragment() {
        // Required empty public constructor
    }

    public static PuntosAcomFragment newInstance(String param1, String param2) {
        PuntosAcomFragment fragment = new PuntosAcomFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_puntos_acom, container, false);
        lista_puntosa = new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_puntosacum);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        llenar_lista();
        adaptadorPuntosA = new AdaptadorPuntosA(getContext(), lista_puntosa);
        recyclerView.setAdapter(adaptadorPuntosA);
        return view;
    }

    private void llenar_lista() {

        lista_puntosa.add(new PuntosA("del 02/02/2019 al 01/03/2019.", "Comisiones por ventas de productos."));
        lista_puntosa.add(new PuntosA("del 02/01/2019 al 01/02/2019.", "Comisiones por compras de productos."));
        lista_puntosa.add(new PuntosA("del 02/12/2018 al 01/01/2019.", "Comisiones por compras de productos."));
        lista_puntosa.add(new PuntosA("del 02/11/2018 al 01/12/2018.", "Comisiones por ventas de productos."));
        lista_puntosa.add(new PuntosA("del 02/10/2018 al 01/11/2018.", "Comisiones por ventas de productos."));
        lista_puntosa.add(new PuntosA("del 02/09/2018 al 01/10/2018.", "Comisiones por adquisición  de membresia."));
        lista_puntosa.add(new PuntosA("del 02/08/2018 al 01/09/2018.", "Comisiones por ventas downline."));
        lista_puntosa.add(new PuntosA("del 02/07/2018 al 01/08/2018.", "Comisiones por ventas downline."));
        lista_puntosa.add(new PuntosA("del 01/06/2018 al 01/07/2018.", "Comisiones por pulicidad downline."));
        lista_puntosa.add(new PuntosA("del 01/05/2018 al 01/06/2018.", "Comisiones por ventas de productos."));
        lista_puntosa.add(new PuntosA("del 01/04/2018 al 01/05/2018.", "Comisiones por ventas de productos."));
        lista_puntosa.add(new PuntosA("del 01/03/2018 al 01/04/2018.", "Comisiones por ventas de productos."));
        lista_puntosa.add(new PuntosA("del 01/02/2018 al 01/03/2018.", "Comisiones por ventas de productos."));
        lista_puntosa.add(new PuntosA("del 01/01/2018 al 01/02/2018.", "Comisiones por compra de productos."));
        lista_puntosa.add(new PuntosA("del 01/02/2018 al 01/03/2018.", "Comisiones por publicidad de productos."));



    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
