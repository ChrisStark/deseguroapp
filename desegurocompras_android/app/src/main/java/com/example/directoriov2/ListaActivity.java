package com.example.directoriov2;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Clases.Session;
import com.example.directoriov2.Clases.Ubicacion;
import com.example.directoriov2.Fragments.Listas.ListaNegociosFragment;
import com.example.directoriov2.Fragments.Listas.ListaProductosFragment;
import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.Modelos.Productos;

public class ListaActivity extends AppCompatActivity  {

    DrawerLayout drawerLayout;
    private Session session;
    NavigationView navigationView;
    Intent intent;
    TextView btnNegocios, btnProductos;
    RelativeLayout selecfrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        init();

    }

    private void init() {
        session = new Session(this);
        btnNegocios = findViewById(R.id.i_ytvii);
        btnProductos = findViewById(R.id.i_dsc);
        selecfrag = findViewById(R.id.select_frag);
        btnProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seleccionFragment(0);

            }
        });
        btnNegocios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seleccionFragment(1);

            }
        });
        cargarToolbar();
        statusLogin();
//        cargarRecycler();
    }

    private void seleccionFragment(int i) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment mifragment = null;
        Negocio negocio;
        Productos producto;
        switch (i) {
            case 0:
                mifragment = new ListaProductosFragment();
                break;
            case 1:
                mifragment = new ListaNegociosFragment();
                break;
        }

        ocultar_frag();

        if(mifragment == null){
            fragmentTransaction.add(R.id.contenedorFragments, mifragment);
            fragmentTransaction.commit();
        }else{
            fragmentTransaction.replace(R.id.contenedorFragments, mifragment);
            fragmentTransaction.commit();
        }

    }

    private void ocultar_frag() {

        selecfrag.setVisibility(View.GONE);

    }

    private void cargarToolbar() {
        ActionBarDrawerToggle toggle;
        Toolbar toolbar;
        toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.navView);
        drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if (navigationView != null) {
            setupNavigationDrawerContent(navigationView);
        }
        setupNavigationDrawerContent(navigationView);
    }

    private void statusLogin() {
        View view;
        Menu menu;
        TextView tvEmail;
        String email;
        CardView cardView;
        view = navigationView.getHeaderView(0);
        cardView = view.findViewById(R.id.CVlogin);
        menu = navigationView.getMenu();

        if (session.getLogeado()) {
            menu.setGroupVisible(R.id.g_session, true);
            tvEmail = (TextView) view.findViewById(R.id.email_user);
            email = session.getEmail();
            tvEmail.setText(email);
            cardView.setVisibility(View.GONE);
            Toast.makeText(this, "usuario logeado" + email, Toast.LENGTH_LONG).show();
        } else {
            menu.setGroupVisible(R.id.g_session, false);
        }
    }

    private void setupNavigationDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        int id = menuItem.getItemId();
                        if (id == R.id.ubicacion)
                            obtenerUbicacion();
                        else if (id == R.id.salir)
                            cerrar_sesion();
                        else {
                            intent = new Intent(ListaActivity.this, MainActivity.class);
                            intent.putExtra("opcion", id);
                            startActivity(intent);
                            //finish();
                        }
                        return true;
                    }
                });
    }

    public void cerrar_sesion() {
        session.setLogeado(false, "", "");
        startActivity(new Intent(this, ListaActivity.class));
        finish();
    }

    private void obtenerUbicacion() {
        ActivityCompat.requestPermissions(ListaActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        if (ActivityCompat.checkSelfPermission(ListaActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(ListaActivity.this, "permiso denegado", Toast.LENGTH_SHORT).show();
        } else {
            Ubicacion ubicacion = new Ubicacion(getApplicationContext());
            ubicacion.getLocation();
        }
    }

    public void onClick(View view) {
        //evento de inicio de session
        switch (view.getId()){

            case R.id.CVlogin:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.yatvii:
                seleccionFragment(1);
                System.out.print("nada");
                break;
            case R.id.desegurocompras:
                seleccionFragment(0);
                System.out.print("nada");
                break;

        }
    }


}