package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.directoriov2.Modelos.Preguntas;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class AdaptadorPreguntas extends RecyclerView.Adapter<AdaptadorPreguntas.ViewHolderPreguntas> {
    ArrayList<Preguntas> listaPreguntas;
    Context context;

    public AdaptadorPreguntas(Context context,ArrayList<Preguntas> listaPreguntas) {
        this.listaPreguntas = listaPreguntas;
        this.context = context;
    }

    @Override
    public AdaptadorPreguntas.ViewHolderPreguntas onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = 0 ;
        layout = R.layout.preguntas_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderPreguntas(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorPreguntas.ViewHolderPreguntas holder, int position) {
        holder.nombre.setText(listaPreguntas.get(position).getNombre());
        holder.leida.setText(listaPreguntas.get(position).getLeida());
        holder.pregunta.setText(listaPreguntas.get(position).getPregunta());

    }

    @Override
    public int getItemCount() {
        return listaPreguntas.size();
    }

    public class ViewHolderPreguntas extends RecyclerView.ViewHolder {
        TextView nombre,pregunta,leida;
        public ViewHolderPreguntas(View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre);
            pregunta = itemView.findViewById(R.id.pregunta);
            leida = itemView.findViewById(R.id.leida);
        }
    }
}
