package com.example.directoriov2.Modelos;


import java.io.Serializable;

public class Productos implements Serializable {
    private static String url = "http://oficina-beta.yatvii.com";
    private int id;
    private int existencia;
    private String image_url;
    private String nombre;
    private String descripcion;
    private String precio;
    private String subdominio;
    private String precio_descuento;


    public Productos(int id, String image_url, String nombre, String descripcion, String precio, int existencia) {
        this.id = id;
        this.image_url = image_url;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.existencia = existencia;
    }
    public Productos(String image_url, String nombre) {
        this.image_url = image_url;
        this.nombre = nombre;
    }

    public String getPrecio_descuento() {
        return precio_descuento;
    }

    public void setPrecio_descuento(String precio_descuento) {
        this.precio_descuento = precio_descuento;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Productos.url = url;
    }

    public String getSubdominio() {
        return subdominio;
    }

    public void setSubdominio(String subdominio) {
        this.subdominio = subdominio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }






}