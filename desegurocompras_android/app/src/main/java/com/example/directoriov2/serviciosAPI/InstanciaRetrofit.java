package com.example.directoriov2.serviciosAPI;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InstanciaRetrofit {

    private static Retrofit retrofit;
    private static Retrofit retrofit2;
    private static Retrofit retrofit3;
   // private static final String BASE_URL_LISTA = "http://yatvii.xyz/";
    private static final String BASE_URL_USUARIOS = "http://oficina-beta.yatvii.com/";
    private static final String BASE_URL_API_OFICINA = "http://api.yatvii.xyz/";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL_API_OFICINA)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getRetrofitInstanceDetalle() {
        if (retrofit2 == null) {
            retrofit2 = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL_API_OFICINA)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit2;
    }

    public static Retrofit getRetrofitInstanceLogin() {

        if (retrofit3 == null) {
            retrofit3 = new retrofit2.Retrofit.Builder()
                    //.baseUrl(BASE_URL_USUARIOS)
                    .baseUrl(BASE_URL_API_OFICINA)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit3;
    }
}