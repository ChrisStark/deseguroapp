package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.directoriov2.Modelos.Compras;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class AdaptadorCompras extends RecyclerView.Adapter<AdaptadorCompras.ViewHolderCompras> {

    private Context context;
    private ArrayList<Compras> compras;

    public AdaptadorCompras (Context context, ArrayList<Compras> compras) {

        this.context = context;
        this.compras = compras;

    }

    @Override
    public ViewHolderCompras onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = 0;
        layout = R.layout.compras_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderCompras(view);

    }

    @Override
    public void onBindViewHolder(ViewHolderCompras holder, int position) {

        holder.nombreneg.setText(compras.get(position).getNombreneg());
        holder.fechac.setText(compras.get(position).getFechac());
        holder.nomart.setText(compras.get(position).getNomart());
        holder.cantidadart.setText(compras.get(position).getCantidadart());
        holder.precioart.setText(compras.get(position).getPrecioart());
        holder.totalcomp.setText(compras.get(position).getTotalcomp());

    }

    @Override
    public int getItemCount() {
        return compras.size();
    }

    public class ViewHolderCompras extends RecyclerView.ViewHolder {

        private TextView nombreneg;
        private TextView fechac;
        private TextView nomart;
        private TextView cantidadart;
        private TextView precioart;
        private TextView totalcomp;

        public ViewHolderCompras(View itemView) {

            super(itemView);
            nombreneg = itemView.findViewById(R.id.c_nombren);
            fechac = itemView.findViewById(R.id.c_fecha);
            nomart = itemView.findViewById(R.id.c_nomart);
            cantidadart = itemView.findViewById(R.id.c_cantart);
            precioart = itemView.findViewById(R.id.c_precioart);
            totalcomp = itemView.findViewById(R.id.c_totalcomp);

        }
    }
}
