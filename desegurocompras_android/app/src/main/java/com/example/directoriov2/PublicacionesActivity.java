package com.example.directoriov2;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.directoriov2.Adaptadores.AdaptadorPublicaciones;
import com.example.directoriov2.Clases.DialogoNuevaPublicacion;
import com.example.directoriov2.Modelos.Publicaciones;

import java.util.ArrayList;

public class PublicacionesActivity extends AppCompatActivity {
    ArrayList<Publicaciones> listaPublicaciones;
    AdaptadorPublicaciones adaptador;
    RecyclerView recycler;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicaciones);

        init();

    }

    private void init() {
        cargarToolbar();
        RecyclerView.LayoutManager layoutManager;
        listaPublicaciones = new ArrayList<>();
        recycler = findViewById(R.id.RVPublicaciones);
        recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(layoutManager);
        llenarPublicaciones();
        adaptador = new AdaptadorPublicaciones(this, listaPublicaciones, new AdaptadorPublicaciones.OnItemClickListener() {
            @Override
            public void onItemClick(Publicaciones item) {
                Toast.makeText(PublicacionesActivity.this, "item " + item.getNombre(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeleteClick(int position) {
                Toast.makeText(PublicacionesActivity.this, "click " + listaPublicaciones.get(position).getNombre(), Toast.LENGTH_SHORT).show();
            }
        });
        recycler.setAdapter(adaptador);
    }

    private void cargarToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }


    private void llenarPublicaciones() {
        String url = "http://oficina-beta.yatvii.com/system/56/articulos/53/original/IE-005.jpg?1553105795";
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
        listaPublicaciones.add(new Publicaciones(1, "laptop", null, url, null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater;
        inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar_publicaciones, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager fm = getSupportFragmentManager();
        DialogoNuevaPublicacion dialogoNuevaPublicacion = new DialogoNuevaPublicacion();
        dialogoNuevaPublicacion.show(fm, DialogoNuevaPublicacion.TAG);
        return super.onOptionsItemSelected(item);
    }


}
