package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.directoriov2.Modelos.PuntosP;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdaptadorPuntosP extends RecyclerView.Adapter<AdaptadorPuntosP.ViewHolderPuntosP> {

    private Context context;
    private ArrayList<PuntosP> puntosp;

    public AdaptadorPuntosP(Context context, ArrayList<PuntosP> puntosp) {

        this.context = context;
        this.puntosp = puntosp;

    }

    @Override
    public AdaptadorPuntosP.ViewHolderPuntosP onCreateViewHolder(ViewGroup parent, int viewType) {
        
        int layout = 0;
        layout = R.layout.puntos_pend_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderPuntosP(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorPuntosP.ViewHolderPuntosP holder, int position) {

        holder.tipo.setText(puntosp.get(position).getTipo());
        holder.nivel.setText(puntosp.get(position).getNivel());
        holder.fecha.setText(puntosp.get(position).getFecha());
        holder.down.setText(puntosp.get(position).getDown());
        holder.descripcion.setText(puntosp.get(position).getDescripcion());
        holder.valorcom.setText(puntosp.get(position).getValorcom());
        holder.porcentaje.setText(puntosp.get(position).getPorcentaje());
        holder.comision.setText(puntosp.get(position).getComision());

    }

    @Override
    public int getItemCount() {

        return puntosp.size();

    }

    public class ViewHolderPuntosP extends RecyclerView.ViewHolder {

        private TextView tipo;
        private TextView nivel;
        private TextView fecha;
        private TextView down;
        private TextView descripcion;
        private TextView valorcom;
        private TextView porcentaje;
        private TextView comision;

        public ViewHolderPuntosP(View itemView) {

            super(itemView);
            tipo = itemView.findViewById(R.id.pp_tipo);
            nivel = itemView.findViewById(R.id.pp_nivel);
            fecha = itemView.findViewById(R.id.pp_fecha);
            down = itemView.findViewById(R.id.pp_down);
            descripcion = itemView.findViewById(R.id.pp_desc);
            valorcom = itemView.findViewById(R.id.pp_valor);
            porcentaje = itemView.findViewById(R.id.pp_porcentaje);
            comision = itemView.findViewById(R.id.pp_comision);

        }
    }

}
