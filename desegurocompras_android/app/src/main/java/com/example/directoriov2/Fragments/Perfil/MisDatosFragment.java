package com.example.directoriov2.Fragments.Perfil;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Clases.Session;
import com.example.directoriov2.Modelos.PerfilUsuario;
import com.example.directoriov2.R;
import com.example.directoriov2.serviciosAPI.APIServices;
import com.example.directoriov2.serviciosAPI.InstanciaRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MisDatosFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    View view;
    TextView noombre,apellido,sexo;
    Session session;
    public MisDatosFragment() {
        // Required empty public constructor
    }

    public static MisDatosFragment newInstance(String param1, String param2) {
        MisDatosFragment fragment = new MisDatosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        session = new Session(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mis_datos, container, false);
        servicioApi();
        return view;
    }
    public void servicioApi(){
        if (session.getLogeado()) {
            String email = session.getEmail();
            String token = session.getToken();
            noombre = view.findViewById(R.id.nombre);
            apellido = view.findViewById(R.id.apellidos);
            sexo = view.findViewById(R.id.sexo);
            APIServices apiServices = InstanciaRetrofit.getRetrofitInstanceLogin().create(APIServices.class);
            Call<PerfilUsuario> call = apiServices.getPerfilUsuario(token,email);
            responsePerfil(call);
        }else {
            Toast.makeText(getActivity(), "no esta logeado", Toast.LENGTH_SHORT).show();
        }
    }

    private void responsePerfil(Call<PerfilUsuario> call) {
        call.enqueue(new Callback<PerfilUsuario>() {
            @Override
            public void onResponse(Call<PerfilUsuario> call, Response<PerfilUsuario> response) {
                if (response.isSuccessful()){
                    noombre.setText(response.body().getNombre());
                    apellido.setText(response.body().getApellido_p()+" "+response.body().getApellido_m());
                    sexo.setText(response.body().getSexo());
                }else{
                    Toast.makeText(getActivity(), "error en formato", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PerfilUsuario> call, Throwable t) {
                Toast.makeText(getActivity(), "hubo un error ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
