package com.example.directoriov2.Fragments.Compras;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.AdaptadorCompras;
import com.example.directoriov2.Modelos.Compras;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class ComprasFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ArrayList<Compras> lista_compras;
    RecyclerView recyclerView;
    AdaptadorCompras adaptadorCompras;

    View view;

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ComprasFragment() {
        // Required empty public constructor
    }

    public static ComprasFragment newInstance(String param1, String param2) {
        ComprasFragment fragment = new ComprasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_compras, container, false);
        lista_compras = new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_compras);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        llenar_lista();

        adaptadorCompras = new AdaptadorCompras(getContext(), lista_compras);
        recyclerView.setAdapter(adaptadorCompras);

        return view;
    }

    private void llenar_lista() {

        lista_compras.add(new Compras("Autos", "07/03/2019", "Celular Xiaomi", "1", "4800.00", "4800.00"));
        lista_compras.add(new Compras("Tecnología", "06/03/2019", "Teclado Eagle", "1", "340.00", "340.00"));
        lista_compras.add(new Compras("Ropa", "05/03/2019", "Blusa Manga Larga", "1", "250.00", "250.00"));
        lista_compras.add(new Compras("Libros", "04/03/2019", "Avengers Infinity War", "10", "44.00", "440.00"));
        lista_compras.add(new Compras("Zapatos Dama", "03/03/2019", "Zapatillas Efe", "1", "699.00", "699.00"));
        lista_compras.add(new Compras("Zapatos Caballero", "02/03/2019", "Bota Discovery", "1", "499.00", "499.00"));
        lista_compras.add(new Compras("Autos", "01/03/2019", "Mazda 3", "1", "350000.00", "350000.00"));
        lista_compras.add(new Compras("Inmuebles", "27/02/2019", "Av. Fresno 5, Bosques de San Juan, San Juan del Río, Querétaro, México, América", "1", "180000.00", "180000.00"));
        lista_compras.add(new Compras("Tecnología", "25/02/2019", "Audífonos Inalámbricos", "1", "420.00", "420.00"));
        lista_compras.add(new Compras("Accesorios", "04/02/2019", "Cargador Celular Mini USB", "1", "190.00", "190.00"));

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
