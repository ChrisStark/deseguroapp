package com.example.directoriov2.Modelos;

public class Preguntas {
    private String pregunta;
    private String leida;
    private String nombre;

    public Preguntas(String pregunta, String leida, String nombre) {
        this.pregunta = pregunta;
        this.leida = leida;
        this.nombre = nombre;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getLeida() {
        return leida;
    }

    public void setLeida(String leida) {
        this.leida = leida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}