package com.example.directoriov2.Fragments.Login;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.directoriov2.R;

public class RegistroFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    TextInputLayout l_nombrep;
    TextInputLayout l_numerop;
    TextInputLayout l_nombreu;
    TextInputLayout l_appat;
    TextInputLayout l_apmat;
    TextInputLayout l_correo;
    TextInputLayout l_numero;
    TextInputLayout l_contraseña;
    TextInputLayout l_confirmar;

    TextInputEditText e_nombrep;
    TextInputEditText e_numerop;
    TextInputEditText e_nombreu;
    TextInputEditText e_appat;
    TextInputEditText e_apmat;
    TextInputEditText e_correo;
    TextInputEditText e_numero;
    TextInputEditText e_contraseña;
    TextInputEditText e_confirmar;

    Button btnRegistrar;

    View view;

    private OnFragmentInteractionListener mListener;

    public RegistroFragment() {
    }

    public static RegistroFragment newInstance(String param1, String param2) {
        RegistroFragment fragment = new RegistroFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_registro, container, false);
        TextView tvlog = (TextView) view.findViewById(R.id.tv_login);
        init();
        tvlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogInFragment logInFragment = new LogInFragment();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.contenedorFragments, logInFragment, logInFragment.getTag()).commit();
            }
        });
        return view;

    }

    private void init() {
        //layouts
        l_nombrep = view.findViewById(R.id.r_lpatrocinador);
        l_numerop = view.findViewById(R.id.r_lnumerop);
        l_nombreu = view.findViewById(R.id.r_lnombreu);
        l_appat = view.findViewById(R.id.r_lappat);
        l_apmat = view.findViewById(R.id.r_lapmat);
        l_correo = view.findViewById(R.id.r_lcorreo);
        l_numero = view.findViewById(R.id.r_lnumero);
        l_contraseña = view.findViewById(R.id.r_lcontraseña);
        l_confirmar = view.findViewById(R.id.r_lconfirmarc);

        //textinputs
        e_nombrep = view.findViewById(R.id.r_epatrocinador);
        e_numerop = view.findViewById(R.id.r_enumerop);
        e_nombreu = view.findViewById(R.id.r_enombreu);
        e_appat = view.findViewById(R.id.r_eappat);
        e_apmat = view.findViewById(R.id.r_eapmat);
        e_correo = view.findViewById(R.id.r_ecorreo);
        e_numero = view.findViewById(R.id.r_enumero);
        e_contraseña = view.findViewById(R.id.r_econtraseña);
        e_confirmar = view.findViewById(R.id.r_econfirmarc);

        btnRegistrar = view.findViewById(R.id.r_btnregistrar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validar_campos();
                String nomp = e_nombrep.getText().toString();
                String nump = e_numerop.getText().toString();
                String nomu = e_nombreu.getText().toString();
                String appat = e_appat.getText().toString();
                String apmat = e_apmat.getText().toString();
                String correo = e_correo.getText().toString();
                String numero = e_numero.getText().toString();
                String contraseña = e_contraseña.getText().toString();
                String confirmar = e_confirmar.getText().toString();

            }
        });

    }


    public void validar_EditText(TextInputLayout float_text, String text) {

        if (!TextUtils.isEmpty(text)) {

            float_text.setError(null);

        }else {

            float_text.setError("Campo Vacío");

        }

    }

    public void validar_campos(){

        validar_EditText(l_nombrep, e_nombrep.getText().toString());
        validar_EditText(l_numerop, e_numerop.getText().toString());
        validar_EditText(l_nombreu, e_nombreu.getText().toString());
        validar_EditText(l_appat, e_appat.getText().toString());
        validar_EditText(l_apmat, e_apmat.getText().toString());
        validar_EditText(l_correo, e_correo.getText().toString());
        validar_EditText(l_numero, e_numero.getText().toString());
        validar_EditText(l_contraseña, e_contraseña.getText().toString());
        validar_EditText(l_confirmar, e_confirmar.getText().toString());

    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
