package com.example.directoriov2.Fragments.ListasDescripcion;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Adaptadores.AdaptadorImagenes;
import com.example.directoriov2.Clases.DialogoAgregarCarrito;
import com.example.directoriov2.Clases.DialogoPreguntas;
import com.example.directoriov2.Clases.ExpandAndCollapseViewUtil;
import com.example.directoriov2.Clases.Session;
import com.example.directoriov2.DescripcionActivity;
import com.example.directoriov2.Interfaces.ComunicacionDialogo;
import com.example.directoriov2.MainActivity;
import com.example.directoriov2.Modelos.CarritoCompras.Carrito;
import com.example.directoriov2.Modelos.DetalleProducto.Detalle;
import com.example.directoriov2.Modelos.DetalleProducto.Imagenes;
import com.example.directoriov2.Modelos.DetalleProducto.ProductoDetalle;
import com.example.directoriov2.Modelos.Productos;
import com.example.directoriov2.R;
import com.example.directoriov2.serviciosAPI.APIServices;
import com.example.directoriov2.serviciosAPI.InstanciaRetrofit;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DescripcionProductosFragment extends Fragment implements View.OnClickListener {
    private static final int DURATION = 250;
    public static String dominio = "http://oficina-beta.yatvii.com";
    private Session session;
    AdaptadorImagenes adaptadorImagenes;
    Productos obj;
    View view;
    int id;
    int longitud;
    String subdominio;
    TextView titulo;
    TextView precio;
    TextView precioDescuento;
    TextView descripcion;
    TextView existencia;
    TextView noFotos;
    ImageView image_expand;
    Button btnComprar;
    ViewPager viewPager;
    LinearLayout LinearDescripcion;
    LinearLayout LinearToggle;
    LinearLayout LinearPrecioDescuento;
    LinearLayout LinearTooglePreguntas;
    Toolbar toolbar;
    CoordinatorLayout coord;
    CollapsingToolbarLayout parallax;
    ArrayList<String> imagenesUrls;
    private OnFragmentInteractionListener mListener;
    public DescripcionProductosFragment() {
        // Required empty public constructor
    }


    public static DescripcionProductosFragment newInstance(Productos producto) {
        DescripcionProductosFragment fragment = new DescripcionProductosFragment();
        Bundle args = new Bundle();
        args.putSerializable("objeto",producto);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            obj = (Productos) getArguments().getSerializable("objeto");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_descripcion_productos, container, false);
        init();
        return view;
    }

    private void init() {
        cargarToolbar();
        session = new Session(getContext());
        coord = view.findViewById(R.id.coord);
        parallax = view.findViewById(R.id.CTL);
        titulo = (TextView) view.findViewById(R.id.titulo);
        descripcion = (TextView) view.findViewById(R.id.descripcion);
        precio = (TextView) view.findViewById(R.id.precio);
        existencia = (TextView) view.findViewById(R.id.existencia);
        noFotos = (TextView) view.findViewById(R.id.noFotos);
        viewPager = view.findViewById(R.id.viewPager);
        btnComprar = view.findViewById(R.id.btnComprar);
        LinearTooglePreguntas = view.findViewById(R.id.LinearTooglePreguntas);
        precioDescuento = view.findViewById(R.id.precioDescuento);
        LinearPrecioDescuento = view.findViewById(R.id.LinearPrecioDescuento);
        image_expand = view.findViewById(R.id.image_expand);
        LinearDescripcion = view.findViewById(R.id.LinearDescripcion);
        LinearToggle = view.findViewById(R.id.LinearToggle);

        imagenesUrls = new ArrayList<>();
        subdominio = obj.getSubdominio();
        id = obj.getId();
        existencia.setText(String.valueOf(obj.getExistencia()));
        titulo.setText(obj.getNombre());
        if (obj.getPrecio_descuento() == null) {
            precio.setText(obj.getPrecio());
            LinearPrecioDescuento.setVisibility(View.GONE);
        } else {
            longitud = obj.getPrecio().length();
            SpannableString mSpannableString = new SpannableString(obj.getPrecio());
            StrikethroughSpan mStrikeThrough = new StrikethroughSpan();
            mSpannableString.setSpan(mStrikeThrough, 0, longitud, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            precio.setText(obj.getPrecio_descuento());
            precioDescuento.setText(mSpannableString);
            LinearPrecioDescuento.setVisibility(View.VISIBLE);
        }
        LinearToggle.setOnClickListener(this);

        respuestaAPI();

        btnComprar.setOnClickListener(this);
        LinearTooglePreguntas.setOnClickListener(this);
    }


    private void respuestaAPI() {
        APIServices apiServices = InstanciaRetrofit.getRetrofitInstanceDetalle().create(APIServices.class);
        Call<ProductoDetalle> call = apiServices.getProductoDetalle(id, subdominio);
        call.enqueue(new Callback<ProductoDetalle>() {
            @Override
            public void onResponse(Call<ProductoDetalle> call, Response<ProductoDetalle> response) {

                if (response.isSuccessful()) {
                    ProductoDetalle productoDetalle = response.body();
                    Detalle detalle = productoDetalle.getDetalle();
                    descripcion.setText(detalle.getDetalle());
                    ArrayList<Imagenes> imagenes = productoDetalle.getImagenes();
                    llenarImagenes(imagenes);

                } else {
                    Toast.makeText(getActivity(), "error formato", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductoDetalle> call, Throwable t) {
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void llenarImagenes(ArrayList<Imagenes> imagenes) {
        for (int i = 0; i < imagenes.size(); i++) {
            imagenesUrls.add(dominio + imagenFormatSmall(imagenes.get(i).getImage_url()));
        }
        if (imagenesUrls.size() < 2) {
            noFotos.append(imagenesUrls.size() + " foto");
        } else {
            noFotos.append(imagenesUrls.size() + " fotos");
        }
        adaptadorImagenes = new AdaptadorImagenes(getActivity(), imagenesUrls);
        viewPager.setAdapter(adaptadorImagenes);
    }

    public String imagenFormatSmall(String img) {
        return img.replace("/original/", "/small/");
    }

    public String imagenFormatTiny(String img) {
        return img.replace("/original/", "/tiny/");
    }


    public void cargarToolbar() {
//        toolbar = view.findViewById(R.id.toolbar);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity)getActivity()).setDisplayShowTitleEnabled(false);
    }


    public void toggleDetails() {
        if (LinearDescripcion.getVisibility() == View.GONE) {
            ExpandAndCollapseViewUtil.expand(LinearDescripcion, DURATION);
            image_expand.setImageResource(R.drawable.ic_expand_more_24dp);
            rotate(-180.0f);
        } else {
            ExpandAndCollapseViewUtil.collapse(LinearDescripcion, DURATION);
            image_expand.setImageResource(R.drawable.ic_expand_less_24dp);
            rotate(180.0f);
        }
    }

    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        image_expand.startAnimation(animation);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        FragmentManager fragmentManager = getFragmentManager();
        switch (id){
            case R.id.btnComprar:
                if (session.getLogeado()){
                    DialogoAgregarCarrito dialogoAgregarCarrito = DialogoAgregarCarrito.newInstance(obj);
                    dialogoAgregarCarrito.show(fragmentManager, "DialogoAgregarCarrito");

                }else{
                    Snackbar.make(view,"no ha iniciado sesion",3000).show();
                }

                break;
            case R.id.LinearTooglePreguntas:
                DialogoPreguntas dialogoPreguntas = new DialogoPreguntas();
                dialogoPreguntas.show(fragmentManager, DialogoPreguntas.TAG);
                break;
            case R.id.LinearToggle:
                toggleDetails();
                break;
        }

    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}