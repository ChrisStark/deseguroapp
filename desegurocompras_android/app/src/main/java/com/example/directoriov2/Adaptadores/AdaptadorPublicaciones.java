package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Modelos.Productos;
import com.example.directoriov2.Modelos.Publicaciones;
import com.example.directoriov2.PublicacionesActivity;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorPublicaciones extends RecyclerView.Adapter<AdaptadorPublicaciones.ViewHolderPublicaciones> {
    private ArrayList<Publicaciones> listaPublicaciones;
    private final OnItemClickListener listener;
    private ActionMode actionMode;

    private Context context;

    public interface OnItemClickListener {
        void onItemClick(Publicaciones item);
        void onDeleteClick(int position);

    }

    public AdaptadorPublicaciones(Context context, ArrayList<Publicaciones> listaPublicaciones, OnItemClickListener listener) {
        this.listaPublicaciones = listaPublicaciones;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderPublicaciones onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        layout = R.layout.publicaciones_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderPublicaciones(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderPublicaciones holder, int position) {
        holder.bind(listaPublicaciones.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return listaPublicaciones.size();
    }

    public class ViewHolderPublicaciones extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {
        TextView titulo, opciones;
        ImageView imagen;

        public ViewHolderPublicaciones(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.nombre);
            opciones = (TextView) itemView.findViewById(R.id.textViewOptions);
            imagen = (ImageView) itemView.findViewById(R.id.imagen);
        }

        public void bind(final Publicaciones item, final OnItemClickListener listener) {
            titulo.setText(item.getNombre());
            Picasso.get()
                    .load(item.getImage_url())
                    .error(R.drawable.i_photo_load)
                    .fit()
                    .centerCrop()
                    .into(imagen);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
            itemView.setOnLongClickListener(this);
            opciones.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(context, opciones, Gravity.RIGHT);
            popup.inflate(R.menu.menu_publicaciones);
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.eliminar:
                            listener.onDeleteClick(getAdapterPosition());
                            break;
                    }
                    return false;
                }
            });
            popup.show();
        }

        @Override
        public boolean onLongClick(View v) {
            if (actionMode!= null){
                return false;
            }
            actionMode = ((PublicacionesActivity)context).startActionMode(mActionModeCallback);
            v.setSelected(true);
            Toast.makeText(context, "long click "+ getAdapterPosition(), Toast.LENGTH_SHORT).show();
            return true;
        }

        public ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_action_mode_publicaciones, menu);//Inflate the menu over action mode

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

                //So here show action menu according to SDK Levels
                if (Build.VERSION.SDK_INT < 11) {
                    MenuItemCompat.setShowAsAction(menu.findItem(R.id.eliminar), MenuItemCompat.SHOW_AS_ACTION_NEVER);
                } else {
                    menu.findItem(R.id.eliminar).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.eliminar:
                        Toast.makeText(context, ""+getAdapterPosition(), Toast.LENGTH_SHORT).show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
            }
        };
    }
}