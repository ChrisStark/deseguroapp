package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Modelos.Uplines;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class AdaptadorUplines extends RecyclerView.Adapter<AdaptadorUplines.ViewHolderUplines> {

    public Context context;
    public ArrayList<Uplines> uplines;
    private AdaptadorUplines.MyClickListener sClickListener;

    public AdaptadorUplines(Context context, ArrayList<Uplines> uplines) {

        this.context = context;
        this.uplines = uplines;

    }

    @Override
    public AdaptadorUplines.ViewHolderUplines onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = 0;
        layout = R.layout.uplines_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderUplines(view);
    }

    @Override
    public void onBindViewHolder(final AdaptadorUplines.ViewHolderUplines holder, final int position) {

        holder.nivel.setText(uplines.get(position).getNivel());
        holder.numpat.setText(uplines.get(position).getNumpat());
        holder.nompat.setText(uplines.get(position).getNompat());

        holder.upline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sClickListener.onDialog(holder.getAdapterPosition());
                // Toast.makeText("lo que sea", Toast.LENGTH_LONG).show();
                Toast.makeText(context, "Item", Toast.LENGTH_SHORT).show();
                // sClickListener.onItemClick(holder.getAdapterPosition(),holder.nompat);
            }
        });

    }

    @Override
    public int getItemCount() {
        return uplines.size();
    }


    public class ViewHolderUplines extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nivel;
        private TextView numpat;
        private TextView nompat;
        private CardView upline;

        public ViewHolderUplines(View itemView) {

            super(itemView);
            nivel = itemView.findViewById(R.id.up_nivel);
            numpat = itemView.findViewById(R.id.up_numpat);
            nompat = itemView.findViewById(R.id.up_nompat);
            upline = itemView.findViewById(R.id.up_upline);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            sClickListener.onItemClick(getAdapterPosition(), v);

        }
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);

        void onDialog(int position);
    }

    public void setOnItemClickListener(AdaptadorUplines.MyClickListener myClickListener) {
        this.sClickListener = myClickListener;

    }


}