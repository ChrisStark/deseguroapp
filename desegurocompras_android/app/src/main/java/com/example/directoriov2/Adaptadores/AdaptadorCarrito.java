package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Modelos.CarritoCompras.Carrito;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class AdaptadorCarrito extends RecyclerView.Adapter<AdaptadorCarrito.ViewHolderCarrito> {
    private Context context;
    private ArrayList<Carrito> carrito;
    private MyClickListener sClickListener;

    public AdaptadorCarrito(Context context, ArrayList<Carrito> carrito) {
        this.context = context;
        this.carrito = carrito;
    }

    @NonNull
    @Override
    public ViewHolderCarrito onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        layout = R.layout.carrito_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderCarrito(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderCarrito holder, int position) {
        Picasso.get()
                .load(carrito.get(position).getImagen())
                .fit()
                .into(holder.imagen);

        holder.nombre.setText(carrito.get(position).getTitulo());
        holder.cantidad.setText(String.valueOf(carrito.get(position).getCantidad()));
        holder.precio.setText(carrito.get(position).getPrecio());
        holder.total.setText(carrito.get(position).getTotal());
        holder.fecha.setText(carrito.get(position).getUpdated_at());
        holder.negocio.setText(carrito.get(position).getNegocio());
        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(context, holder.options, Gravity.RIGHT);
                popup.inflate(R.menu.options);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.eliminar:
                                  sClickListener.onDeleteClick(holder.getAdapterPosition());
                                break;
                            case R.id.comprar:
                                sClickListener.onComprarClick(holder.getAdapterPosition());
                                Toast.makeText(context, "comprar", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return false;
                    }
                });
                popup.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return carrito.size();
    }

    public class ViewHolderCarrito extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView fecha, nombre, precio, total, cantidad, negocio, options;
        ImageView imagen;

        public ViewHolderCarrito(View itemView) {
            super(itemView);
            options = itemView.findViewById(R.id.textViewOptions);
            imagen = itemView.findViewById(R.id.imagen);
            fecha = itemView.findViewById(R.id.fecha);
            nombre = itemView.findViewById(R.id.nombre);
            precio = itemView.findViewById(R.id.precio);
            total = itemView.findViewById(R.id.total);
            cantidad = itemView.findViewById(R.id.cantidad);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            sClickListener.onItemClick(getAdapterPosition(), v);
        }
    }



    public interface MyClickListener {
        void onItemClick(int position, View v);
        void onDeleteClick(int position);
        void onComprarClick(int position);
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.sClickListener = myClickListener;

    }

}