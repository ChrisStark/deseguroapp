package com.example.directoriov2.Modelos;

public class Uplines {

    private String nivel;
    private String numpat;
    private String nompat;

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNumpat() {
        return numpat;
    }

    public void setNumpat(String numpat) {
        this.numpat = numpat;
    }

    public String getNompat() {
        return nompat;
    }

    public void setNompat(String nompat) {
        this.nompat = nompat;
    }

    public Uplines(String nivel, String numpat, String nompat) {
        this.nivel = nivel;
        this.numpat = numpat;
        this.nompat = nompat;
    }
}
