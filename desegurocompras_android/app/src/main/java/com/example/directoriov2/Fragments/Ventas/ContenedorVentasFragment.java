package com.example.directoriov2.Fragments.Ventas;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.ViewPagerFragmentAdapter;
import com.example.directoriov2.Fragments.Compras.ComprasFragment;
import com.example.directoriov2.Fragments.Compras.ComprasPendFragment;
import com.example.directoriov2.R;


public class ContenedorVentasFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private View view;
    ViewPager viewPager;
    TabLayout tabLayout;
    ViewPagerFragmentAdapter viewPagerFragmentAdapter;

    public ContenedorVentasFragment() {
        // Required empty public constructor
    }

    public static ContenedorVentasFragment newInstance(String param1, String param2) {
        ContenedorVentasFragment fragment = new ContenedorVentasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_contenedor_ventas, container, false);

        tabLayout = view.findViewById(R.id.tabLayout_ventas);
        viewPager = view.findViewById(R.id.viewPagerVentas);
        viewPagerFragmentAdapter = new ViewPagerFragmentAdapter((getFragmentManager()));
        viewPagerFragmentAdapter.addFragment(new VentasFragment(),"Ventas");
        viewPagerFragmentAdapter.addFragment(new VentasPendFragment(),"Ventas Pendientes");
        viewPager.setAdapter(viewPagerFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
