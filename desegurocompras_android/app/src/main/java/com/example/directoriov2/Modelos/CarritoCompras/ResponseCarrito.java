package com.example.directoriov2.Modelos.CarritoCompras;

import java.util.ArrayList;
import java.util.List;

public class ResponseCarrito {

    private ModelNegocio negocio;
    private Compra compra;
    private ArrayList<Items> items = null;

    public ModelNegocio getNegocio() {
        return negocio;
    }

    public void setNegocio(ModelNegocio negocio) {
        this.negocio = negocio;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public ArrayList<Items> getItems() {
        return items;
    }

    public void setItems(ArrayList<Items> items) {
        this.items = items;
    }

}