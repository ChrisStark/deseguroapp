package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorNegocios extends RecyclerView.Adapter<AdaptadorNegocios.ViewHolderNegocios> {

    private Context context;
    private ArrayList<Negocio> negocios;
    private final AdaptadorNegocios.OnItemClickListener listener;

    public interface OnItemClickListener {

        void onItemClick(Negocio item);

    }

    public AdaptadorNegocios(Context context, ArrayList<Negocio> negocios, OnItemClickListener listener) {
        this.context = context;
        this.negocios = negocios;
        this.listener = listener;
    }

    @Override
    public AdaptadorNegocios.ViewHolderNegocios onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = 0;

        layout = R.layout.negocios_row_items;

        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);

        return new ViewHolderNegocios(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorNegocios.ViewHolderNegocios holder, int position) {

        holder.bind(negocios.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return negocios.size();
    }

    public class ViewHolderNegocios extends RecyclerView.ViewHolder {

        ImageView imagen;
        TextView titulo;
        TextView descrip;

        public ViewHolderNegocios(View itemView) {

            super(itemView);
            imagen = itemView.findViewById(R.id.i_negocios);
            descrip = itemView.findViewById(R.id.tv_desneg);
            titulo = itemView.findViewById(R.id.tv_nomneg);

        }

        public void bind(final Negocio item, final OnItemClickListener listener) {

            titulo.setText(item.getTitulo());
            descrip.setText(item.getDescrip());

            Picasso.get()
                    .load(item.getImagen())
                    .into(imagen);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });

        }

    }
}