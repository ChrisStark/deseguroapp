package com.example.directoriov2;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Adaptadores.AdaptadorImagenes;
import com.example.directoriov2.Clases.DialogoAgregarCarrito;
import com.example.directoriov2.Clases.DialogoConcretarCompra;
import com.example.directoriov2.Clases.DialogoPreguntas;
import com.example.directoriov2.Clases.ExpandAndCollapseViewUtil;
import com.example.directoriov2.Clases.Session;
import com.example.directoriov2.Fragments.Listas.ListaProductosFragment;
import com.example.directoriov2.Fragments.ListasDescripcion.DescripcionNegUserFragment;
import com.example.directoriov2.Fragments.ListasDescripcion.DescripcionNegociosFragment;
import com.example.directoriov2.Fragments.ListasDescripcion.DescripcionProductosFragment;
import com.example.directoriov2.Interfaces.ComunicacionDialogo;
import com.example.directoriov2.Modelos.CarritoCompras.Carrito;
import com.example.directoriov2.Modelos.DetalleProducto.Detalle;
import com.example.directoriov2.Modelos.DetalleProducto.Imagenes;
import com.example.directoriov2.Modelos.DetalleProducto.ProductoDetalle;
import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.Modelos.Preguntas;
import com.example.directoriov2.Modelos.Productos;
import com.example.directoriov2.serviciosAPI.APIServices;
import com.example.directoriov2.serviciosAPI.InstanciaRetrofit;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DescripcionActivity extends AppCompatActivity implements ComunicacionDialogo {
    AdaptadorImagenes adaptadorImagenes;
    public static String TAG_LISTA_PRODUCTOS = "listaProductos";
    public static String TAG_LISTA_NEGOCIOS = "listaNegocios";
    public static String TAG_LISTA_NEGOCIOS_USUARIO = "listaNegociosUsuario";
    public String tag;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion);
        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("tag");
            if (tag.equals(TAG_LISTA_PRODUCTOS)) {
                seleccionFragment(0);
            } else if (tag.equals(TAG_LISTA_NEGOCIOS)) {
                seleccionFragment(1);

            }else if (tag.equals(TAG_LISTA_NEGOCIOS_USUARIO)) {
                seleccionFragment(2);

            }
        }
    }

    private void seleccionFragment(int i) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment mifragment = null;
        Negocio negocio;
        Productos producto;
        switch (i) {
            case 0:
                producto = (Productos) getIntent().getExtras().getSerializable("objeto");
                new DescripcionProductosFragment();
                mifragment = DescripcionProductosFragment.newInstance(producto);
                break;
            case 1:
                negocio = (Negocio) getIntent().getExtras().getSerializable("objeto");
                new DescripcionNegociosFragment();
                mifragment = DescripcionNegociosFragment.newInstance(negocio);
                break;
            case 2: negocio = (Negocio) getIntent().getExtras().getSerializable("objeto");
                new DescripcionNegUserFragment();
                mifragment = DescripcionNegUserFragment.newInstance(negocio);
        }
        if(mifragment == null){
            fragmentTransaction.add(R.id.contenedorFragments, mifragment);
            fragmentTransaction.commit();
        }else{
            fragmentTransaction.replace(R.id.contenedorFragments, mifragment);
            fragmentTransaction.commit();
        }


    }

    @Override
    public void addCarrito(Carrito carrito) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("opcion", 1);
        startActivity(intent);
        finish();
        Toast.makeText(this, "Se agrego al carrito", Toast.LENGTH_SHORT).show();
    }

}