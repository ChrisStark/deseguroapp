package com.example.directoriov2.Fragments.Compras;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.directoriov2.Adaptadores.AdaptadorCarrito;
import com.example.directoriov2.Adaptadores.SeccionCarritoCompras;
import com.example.directoriov2.Clases.DialogoComprarEliminar;
import com.example.directoriov2.Clases.DialogoConcretarCompra;
import com.example.directoriov2.Clases.Session;
import com.example.directoriov2.ListaActivity;
import com.example.directoriov2.Modelos.CarritoCompras.Carrito;
import com.example.directoriov2.Modelos.CarritoCompras.Compra;
import com.example.directoriov2.Modelos.CarritoCompras.Item;
import com.example.directoriov2.Modelos.CarritoCompras.Items;
import com.example.directoriov2.Modelos.CarritoCompras.ModelNegocio;
import com.example.directoriov2.Modelos.CarritoCompras.Publicacion;
import com.example.directoriov2.Modelos.CarritoCompras.ResponseCarrito;
import com.example.directoriov2.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComprasPendFragment extends Fragment {
    View view;
    ArrayList<Carrito> listaCarrito;
    ArrayList<String> listaNegocios;
    RecyclerView recycler;
    AdaptadorCarrito adaptador;
    SectionedRecyclerViewAdapter sectionAdapter;
    SeccionCarritoCompras seccionCarritoCompras;
    Session session;
    ActionBar actionBar;
    private CoordinatorLayout coordinatorLayout;

    private OnFragmentInteractionListener mListener;

    public ComprasPendFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_compras_pend, container, false);
        cargarToolbar();
        init();

        return view;

    }

    private void init() {
        coordinatorLayout = view.findViewById(R.id.coord);
        listaCarrito = new ArrayList<>();
        session = new Session(getContext());
        respuestaAPI();

        Set hs = new HashSet(); //limpia repetidos
        listaNegocios = new ArrayList<>();
        for (int i = 0; i < listaCarrito.size(); i++) {
            listaNegocios.add(listaCarrito.get(i).getNegocio());
        }
        hs.addAll(listaNegocios);
        listaNegocios.clear();
        listaNegocios.addAll(hs);

        ArrayList<Carrito> lista = new ArrayList<>();
        sectionAdapter = new SectionedRecyclerViewAdapter();

        String sectionTag;
        for (int i = 0; i < listaNegocios.size(); i++) {
            sectionTag = String.format("section%sTag", listaNegocios.get(i).toString());
            //sectionTag = listaNegocios.get(i).toString();
            System.out.println("lista : " + listaNegocios);
            lista = getNegociosPorNombre(listaNegocios.get(i));
            AccionesAdapter(sectionTag,listaNegocios, lista, i);

        }

        recycler = view.findViewById(R.id.RVcarrito);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(sectionAdapter);
    }

    private void AccionesAdapter(final String sectionTag, ArrayList<String> listaNegocios, final ArrayList<Carrito> lista, int i) {
        sectionAdapter.addSection(new SeccionCarritoCompras(getContext(),sectionTag, lista, listaNegocios.get(i),
                new SeccionCarritoCompras.MyClickListener() {
                    @Override
                    public void onItemClick(Carrito carrito) {
                        Toast.makeText(getActivity(), "item", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDeleteClick(int index) {
                         //deleteItem(index,sectionTag);
                        int sectionPos = sectionAdapter.getSectionPosition(sectionTag);
                        lista.remove(sectionPos);
                        sectionAdapter.notifyItemRemovedFromSection(sectionTag, sectionPos);

//                        String name = listaCarrito.get(index).getTitulo();
//                        final Carrito deletedItem = listaCarrito.get(index);
//                        final int deletedIndex = index;
//                        deleteItem(index,sectionTag);
//                        System.out.println("posicion del lista" + deletedItem.getTitulo());
//                        System.out.println("posicion de borrado" + deletedIndex);
//                        Snackbar snackbar = Snackbar
//                                .make(coordinatorLayout, name + "Eliminado del carrito!", Snackbar.LENGTH_LONG);
//                        snackbar.setAction("Deshacer", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                restoreItem(deletedItem, deletedIndex);
//                                System.out.println("posicion del lista 2" + deletedItem.getTitulo());
//                                System.out.println("posicion de borrado 2" + deletedIndex);
//                            }
//                        });
//                        snackbar.setActionTextColor(Color.YELLOW);
//                        snackbar.show();
                    }

                    @Override
                    public void onComprarClick(int position) {
                        DialogoConcretarCompra dialog = new DialogoConcretarCompra();
                        Bundle b = new Bundle();
                        b.putSerializable("objeto", listaCarrito.get(position));
                        dialog.setArguments(b);
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        dialog.show(ft, DialogoConcretarCompra.TAG);

                    }
                }));
    }

    private ArrayList<Carrito> getNegociosPorNombre(String negocio) {
        ArrayList<Carrito> lista = new ArrayList<>();

        for (int i = 0; i < listaCarrito.size(); i++) {
            if (negocio.equals(listaCarrito.get(i).getNegocio())) {
                lista.add(listaCarrito.get(i));
            }
        }
        return lista;

    }

    public void deleteItem(int index,String TAG) {
       // if (index != RecyclerView.NO_POSITION) {
            int positionInSection = sectionAdapter.getPositionInSection(index);

            listaCarrito.remove(positionInSection);
            sectionAdapter.notifyItemRemovedFromSection(TAG, positionInSection);
     //   }

//        listaCarrito.remove(index);
//        sectionAdapter.notifyItemRemoved(index);
    }

    public void restoreItem(Carrito item, int position) {
        listaCarrito.add(position, item);
        sectionAdapter.notifyItemInserted(position);
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void respuestaAPI() {
        if (session.getLogeado()) {
            String token = session.getToken();
            String email = session.getEmail();
 /*           APIServices apiServices = InstanciaRetrofit.getRetrofitInstanceLogin().create(APIServices.class);
            Call<ArrayList<ResponseCarrito>> call = apiServices.getCarrito(token, email);
            responseCarrito(call);*/
            llenar();
        } else {
            startActivity(new Intent(getActivity(), ListaActivity.class));
            getActivity().finish();
        }
    }

    private void responseCarrito(Call<ArrayList<ResponseCarrito>> call) {
        call.enqueue(new Callback<ArrayList<ResponseCarrito>>() {
            @Override
            public void onResponse(Call<ArrayList<ResponseCarrito>> call, Response<ArrayList<ResponseCarrito>> response) {
                if (response.isSuccessful()) {
                    ArrayList<ResponseCarrito> res = response.body();
                    ModelNegocio negocio;
                    Publicacion publicacion;
                    Item item;
                    ArrayList<Items> items;
                    Compra compra;
                    int cantidad;
                    String n_negocio, imagen, descripcion, fecha, precio, total, subdominio;
                    for (int x = 0; x < res.size(); x++) {
                        items = res.get(x).getItems();
                        compra = res.get(x).getCompra();
                        negocio = res.get(x).getNegocio();
                        for (int y = 0; y < items.size(); y++) {
                            item = items.get(y).getItem();
                            n_negocio = negocio.getNombre();
                            publicacion = items.get(y).getPublicacion();
                            descripcion = item.getDescripcion();
                            precio = item.getUnitario();
                            fecha = compra.getUpdated_at();
                            imagen = publicacion.getImage_url();
                            cantidad = item.getCantidad();
                            total = compra.getTotal();
                            imagen = "http://oficina-beta.yatvii.com" + formatoImagenTiny(imagen);
                            Carrito carrito = new Carrito(imagen, descripcion, fecha, cantidad, precio, total, n_negocio);
                            llenarCarrito(carrito);
                        }
                    }
                    sectionAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "error en los datos", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ResponseCarrito>> call, Throwable t) {
                Toast.makeText(getActivity(), "hubo un error", Toast.LENGTH_SHORT).show();
                System.out.println("1: " + t.toString());
            }
        });
    }

    public void llenarCarrito(Carrito carrito) {
        listaCarrito.add(carrito);
    }

    public void llenar() {
        String url ="http://oficina-beta.yatvii.com/system/2/articulos/1/small/IH-1.png?1553787692";
        String titulo = "Celular Xiaomi Redmi Note 6 Pro 64gb 4gb Ram Versión Global";
        listaCarrito.add(new Carrito(url, "electronica 1", "10/10/19", 10, "100", "1000", "electronica"));
        listaCarrito.add(new Carrito(url, "prendas 1", "10/10/19", 10, "100", "1000", "prendas"));
        listaCarrito.add(new Carrito(url, "herramientas 1", "10/10/19", 10, "100", "1000", "herramientas"));
        listaCarrito.add(new Carrito(url, "prendas 2", "10/10/19", 10, "100", "1000", "prendas"));
        listaCarrito.add(new Carrito(url, "herramientas 2", "10/10/19", 10, "100", "1000", "herramientas"));
        listaCarrito.add(new Carrito(url, "electronica 2", "10/10/19", 10, "100", "1000", "electronica"));


    }

    public String formatoImagenTiny(String imagen) {
        return imagen.replace("/original/", "/tiny/");
    }

    public void cargarToolbar() {
//        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.opciones_carrito, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ComprarTodo:
                action(0);
                return true;
            case R.id.EliminarTodo:
                action(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void action(int action) {
        DialogoComprarEliminar dialog = new DialogoComprarEliminar();
        Bundle b = new Bundle();
        b.putStringArrayList("nombres_negocios", listaNegocios);
        b.putInt("action",action);
        dialog.setArguments(b);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialog.show(ft, DialogoComprarEliminar.TAG);
    }
}