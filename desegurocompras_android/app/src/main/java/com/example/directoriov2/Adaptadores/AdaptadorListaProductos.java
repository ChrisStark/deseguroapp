package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.directoriov2.Modelos.Productos;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorListaProductos
        extends RecyclerView.Adapter<AdaptadorListaProductos.ViewHolderProductos> implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(Productos item);

    }

    private ArrayList<Productos> listaProductos;
    private ArrayList<Productos> listaProductosfilter;
    private final OnItemClickListener listener;

    private Context context;


    public AdaptadorListaProductos(ArrayList<Productos> listaProductos, Context context, OnItemClickListener listener) {
        this.listaProductos = listaProductos;
        this.listaProductosfilter = listaProductos;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderProductos onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        int layout = 0;
        layout = R.layout.lista_productos_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderProductos(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderProductos holder, int position) {
        holder.bind(listaProductos.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return listaProductos.size();
    }


    public class ViewHolderProductos extends RecyclerView.ViewHolder {
        TextView titulo, precio, precioDescuento,label_descuento;
        ImageView imagen;
        LinearLayout linearLayout;
        SpannableString mSpannableString;
        StrikethroughSpan mStrikeThrough;
        int longitud;

        public ViewHolderProductos(@NonNull View itemView) {
            super(itemView);
            label_descuento = itemView.findViewById(R.id.label_descuento);
            precioDescuento = (TextView) itemView.findViewById(R.id.precioDescuento);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            precio = (TextView) itemView.findViewById(R.id.precio);
            imagen = (ImageView) itemView.findViewById(R.id.imagen);
            linearLayout = itemView.findViewById(R.id.layout_precioDescuento);
        }

        public void bind(final Productos item, final OnItemClickListener listener) {
            if (item.getPrecio_descuento() == null) {
                linearLayout.setVisibility(View.INVISIBLE);
                precio.setText(item.getPrecio());
                label_descuento.setVisibility(View.GONE);

            } else {
                label_descuento.setVisibility(View.VISIBLE);
                longitud = item.getPrecio().length();
                mSpannableString = new SpannableString(item.getPrecio());
                mStrikeThrough = new StrikethroughSpan();
                mSpannableString.setSpan(mStrikeThrough, 0, longitud, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                precioDescuento.setText(mSpannableString);
                linearLayout.setVisibility(View.VISIBLE);
                precio.setText(item.getPrecio_descuento());
                // precioDescuento.setText(item.getPrecio());

            }

            titulo.setText(item.getNombre());

            Picasso.get()
                    .load(item.getImage_url())
                    .error(R.drawable.i_photo_load)
                    .into(imagen);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                String url = "http://desegurocompras.ytv.mx/imagenes/no-resultados.jpg";
                String nombre = "no se encontraron resultados";
                if (charString.isEmpty()) {
                    listaProductos = listaProductosfilter;
                } else {
                    ArrayList<Productos> filteredList = new ArrayList<>();
                    for (Productos row : listaProductosfilter) {
                        if (row.getNombre().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    if (filteredList.isEmpty()) {
                        Productos vacio = new Productos(url, nombre);
                        filteredList.add(vacio);
                    }
                    listaProductos = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listaProductos;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listaProductos = (ArrayList<Productos>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


}
