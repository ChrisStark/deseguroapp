package com.example.directoriov2.Clases;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public Session(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences("DeseguroCompras",Context.MODE_PRIVATE);
        editor = preferences.edit();
    }
    public void setLogeado(boolean logeado,String token,String email){
        editor.putBoolean("logeado",logeado);
        editor.putString("token",token);
        editor.putString("email",email);
        editor.commit();
    }

    public boolean getLogeado(){
        return preferences.getBoolean("logeado",false);
    }

    public  String getToken(){
        return preferences.getString("token","");
    }
    public String getEmail(){
        return preferences.getString("email","");
    }
}
