package com.example.directoriov2.Clases;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class DialogoComprarEliminar extends DialogFragment {

    public static String TAG = "DialogoComprarEliminar";
    Button btnComprarTodo;
    TextView no_productos;
    TextView totalProductos;
    ArrayList<String> listaNegocios;
    int action;
    Spinner spinner;
    View view;

    public DialogoComprarEliminar() {
    }

    public static DialogoComprarEliminar newInstance(ArrayList<String> listaNegocios, int action) {
        DialogoComprarEliminar frag = new DialogoComprarEliminar();
        Bundle args = new Bundle();
        args.putStringArrayList("nombres_negocios", listaNegocios);
        args.putInt("action", action);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //   setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogo_comprar_eliminar, container, false);
        Bundle b = getArguments();
        listaNegocios = b.getStringArrayList("nombres_negocios");
        action = b.getInt("action");
        init();
        return view;
    }

    private void init() {
        no_productos = view.findViewById(R.id.no_productos);
        totalProductos = view.findViewById(R.id.totalProductos);
        btnComprarTodo = view.findViewById(R.id.btnComprarTodo);
        spinner = (Spinner) view.findViewById(R.id.spinnerNegocios);
        spinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listaNegocios));
        llenarCampos();
        if (action == 0) {
            btnComprarTodo.setText("Comprar todo");
        } else if (action == 1) {
            btnComprarTodo.setText(" Eliminar Todo");
        }


    }

    private void llenarCampos() {

    }
}
