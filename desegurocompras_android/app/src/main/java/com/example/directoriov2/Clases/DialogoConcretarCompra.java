package com.example.directoriov2.Clases;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.directoriov2.Modelos.CarritoCompras.Carrito;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

public class DialogoConcretarCompra extends DialogFragment {
    public static String TAG = "DialogoConcretarCompra";
    TextView nomNegocio,direccionNegocio,sitioWeb,nombreComprador,correoComprador,
             total,nombreProducto,cantidad,unitario,totalDesc;
    EditText et_camtidad;
    Button btnConcretar;
    ImageView imagen;
    View view;
    Carrito carrito;
    public DialogoConcretarCompra() {
    }


    public static DialogoConcretarCompra newInstance(Carrito carrito) {
        DialogoConcretarCompra frag = new DialogoConcretarCompra();
        Bundle args = new Bundle();
        args.putSerializable("objeto",carrito);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setStyle(DialogFragment.STYLE_NORMAL,R.style.EstiloFullScreen);
      //  setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogo_concretar_compra, container, false);
        Bundle b = getArguments();
        carrito = (Carrito) b.getSerializable("objeto");
        setToolbar();
        init();
        return view;
    }

    private void setToolbar() {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setTitle("Concretar Compra");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
              //  Toast.makeText(getActivity(), "clcik", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void init() {
        nomNegocio = view.findViewById(R.id.nomNegocio);
        direccionNegocio = view.findViewById(R.id.direccionNegocio);
        sitioWeb = view.findViewById(R.id.sitioWeb);
        nombreComprador = view.findViewById(R.id.nombreComprador);
        correoComprador = view.findViewById(R.id.correoComprador);
        total = view.findViewById(R.id.total);
        nombreProducto= view.findViewById(R.id.nombreProducto);
        cantidad = view.findViewById(R.id.cantidad);
        unitario = view.findViewById(R.id.unitario);
        totalDesc = view.findViewById(R.id.totalDescripcion);

        et_camtidad = view.findViewById(R.id.et_Cantidad);
        btnConcretar = view.findViewById(R.id.btnConcretar);
        imagen= view.findViewById(R.id.imagen);

        nomNegocio.setText(carrito.getNegocio());
      /*  direccionNegocio.setText(carrito.getDireccionNegocio);
        sitioWeb.setText(carrito.getSitioWeb);
        nombreComprador.setText(carrito.getNombreComprador);
        correoComprador.setText(carrito.getCorreoComprador);*/
        total.setText(carrito.getTotal());
        nombreProducto.setText(carrito.getTitulo());
        cantidad.setText(String.valueOf(carrito.getCantidad()));
        unitario.setText(carrito.getPrecio());
        totalDesc.setText(carrito.getTotal());

        Picasso.get()
                .load(carrito.getImagen())
                .into(imagen);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

}
