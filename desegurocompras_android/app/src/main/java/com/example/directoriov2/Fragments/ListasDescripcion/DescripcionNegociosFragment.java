package com.example.directoriov2.Fragments.ListasDescripcion;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.directoriov2.Clases.DialogoMapas;
import com.example.directoriov2.Clases.ExpandAndCollapseViewUtil;
import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;


public class DescripcionNegociosFragment extends Fragment implements View.OnClickListener {
    private static final int DURATION = 250;
    public static String dominio = "http://oficina-beta.yatvii.com";
    Negocio obj;
    View view;
    TextView titulo;
    ImageView image_expand;
    ImageView imagen;
    LinearLayout LinearToggleDireccion;
    LinearLayout LinearDescripcion;
    LinearLayout LinearToggleDescripcion;
    private OnFragmentInteractionListener mListener;

    public DescripcionNegociosFragment() {

    }

    public static DescripcionNegociosFragment newInstance(Negocio negocio) {
        DescripcionNegociosFragment fragment = new DescripcionNegociosFragment();
        Bundle args = new Bundle();
        args.putSerializable("objeto", negocio);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            obj = (Negocio) getArguments().getSerializable("objeto");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_descripcion_negocios, container, false);
        init();
        return view;
    }

    private void init() {
        LinearToggleDireccion = view.findViewById(R.id.LinearToggleDireccion);
        LinearDescripcion = view.findViewById(R.id.LinearDescripcion);
        LinearToggleDescripcion = view.findViewById(R.id.LinearToggleDescripcion);
        image_expand = view.findViewById(R.id.image_expand);
        titulo = view.findViewById(R.id.titulo);
        imagen = view.findViewById(R.id.imagen);
        Picasso.get()
                .load(obj.getImagen())
                .error(R.drawable.i_photo_load)
                .fit()
                .into(imagen);
        titulo.setText(obj.getTitulo());
        LinearToggleDireccion.setOnClickListener(this);
        LinearToggleDescripcion.setOnClickListener(this);
    }

    public void toggleDescripcion() {
        if (LinearDescripcion.getVisibility() == View.GONE) {
            ExpandAndCollapseViewUtil.expand(LinearDescripcion, DURATION);
            image_expand.setImageResource(R.drawable.ic_expand_more_24dp);
            rotate(-180.0f);
        } else {
            ExpandAndCollapseViewUtil.collapse(LinearDescripcion, DURATION);
            image_expand.setImageResource(R.drawable.ic_expand_less_24dp);
            rotate(180.0f);
        }
    }

    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        image_expand.startAnimation(animation);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        FragmentManager fragmentManager = getFragmentManager();
        switch (id) {
            case R.id.LinearToggleDireccion:
                DialogoMapas dialogoPreguntas = new DialogoMapas();
                dialogoPreguntas.show(fragmentManager, DialogoMapas.TAG);
                break;
            case R.id.LinearToggleDescripcion:
                toggleDescripcion();
                break;
        }
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
