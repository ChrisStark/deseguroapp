package com.example.directoriov2.Clases;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.directoriov2.Fragments.Negocios.NegociosFragment;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class DialogoModificarNegocio extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    public static String TAG = "Dialogo Modificar Negocio";

    TextInputEditText nombreneg;
    TextInputEditText url_etx;
    TextInputEditText desc_neg;
    TextInputEditText correo;
    TextInputEditText numero;

    TextInputLayout l_nombre;
    TextInputLayout l_url;
    TextInputLayout l_desc;
    TextInputLayout l_correo;
    TextInputLayout l_numero;

    Button btn_aceptar;
    Button btn_cancelar;
    Button btn_imagen;

    Spinner spinner;
    Spinner spinner2;

    ArrayList<String> al_cat1;
    ArrayList<String> al_cat2;

    int action;

    View view;

    public DialogoModificarNegocio() {

    }

    public static DialogoModificarNegocio newInstance(ArrayList<String> al_cat1) {

        DialogoModificarNegocio frag = new DialogoModificarNegocio();
        Bundle args = new Bundle();
        args.putStringArrayList("objeto", al_cat1);
        frag.setArguments(args);
        return frag;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        setStyle(DialogFragment.STYLE_NORMAL, R.style.EstiloFullScreen);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogo_modificar_negocio, container, false);
        Bundle b = getArguments();
        al_cat1 = new ArrayList<>();
        al_cat1 = b.getStringArrayList("objeto");
        al_cat2 = new ArrayList<>();
        al_cat2 = b.getStringArrayList("objeto");
        setToolbar();
        init();
        return view;

    }

    private void setToolbar() {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setTitle("Modificar Datos del Negocio");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void init() {

        l_nombre = view.findViewById(R.id.lo_nombre);
        l_url = view.findViewById(R.id.lo_url);
        l_desc = view.findViewById(R.id.lo_desc);
        l_correo = view.findViewById(R.id.lo_correo);
        l_numero = view.findViewById(R.id.lo_numero);

        nombreneg = view.findViewById(R.id.neg_nombre);
        url_etx = view.findViewById(R.id.neg_urle);
        desc_neg = view.findViewById(R.id.neg_desc);
        correo = view.findViewById(R.id.neg_correo);
        numero = view.findViewById(R.id.neg_numero);
        btn_imagen = view.findViewById(R.id.neg_btnimg);

        btn_imagen.setOnClickListener(this);
        btn_aceptar = view.findViewById(R.id.neg_aceptar);
        btn_cancelar = view.findViewById(R.id.neg_cancelar);

        spinner = (Spinner) view.findViewById(R.id.neg_cat1);
        spinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, al_cat1));

        spinner2 = (Spinner) view.findViewById(R.id.neg_cat2);
        spinner2.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, al_cat2));

        llenarSpinner();

        btn_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validar_campos();

                String nombren = nombreneg.getText().toString();
                String url = url_etx.getText().toString();
                String descrip = desc_neg.getText().toString();
                String email = correo.getText().toString();
                String numeron = numero.getText().toString();

            }
        });

    }

    private void llenarSpinner() {

        ArrayList<String> lista = new ArrayList<>();

        lista.add("Dónde Comer...");
        lista.add("Animales y Plantas");
        lista.add("Arte y Anigüedades");
        lista.add("Alimentos y Bebidas");
        lista.add("Ropa, Calzado y Accesorios");
        lista.add("Productos de Belleza");
        lista.add("Servicios Médicos y de Bienestar");
        lista.add("Artículos de Ecuela y Oficina");
        lista.add("Computación y Electrónica");
        lista.add("Educacuón y Cursos");
        lista.add("Libros");
        lista.add("Hogar");
        lista.add("Bienes Raíces (Renta o Venta)");
        lista.add("Transporte");
        lista.add("Autos, Motos y Accesorios");
        lista.add("Entretenimiento");
        lista.add("Viajes");
        lista.add("Eventos y Fiestas");
        lista.add("Seguros y Artículos de Seguridad");
        lista.add("Organizaciones para Donar");
        lista.add("Inversiones");
        lista.add("Artículos de Ocasión (Nuevos y Semi Nuevos)");
        lista.add("Servicios Varios");
        lista.add("Herramientas y Maquinaría");
        lista.add("Materia Prima");
        lista.add("Publicidad");
        lista.add("Deportes");

        ArrayAdapter<String> adaptadorCat1;
        ArrayAdapter<String> adaptadorCat2;

        adaptadorCat1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, lista);
        adaptadorCat2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, lista);

        spinner.setAdapter(adaptadorCat1);
        spinner2.setAdapter(adaptadorCat2);

        spinner.setPrompt("Elija una Categoría");
        spinner2.setPrompt("Elija una Categoría");

        spinner.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(this);

    }

    public void validar_Edittext(TextInputLayout float_text, String text) {

        if (!TextUtils.isEmpty(text)) {

            float_text.setError(null);

        }else {

            float_text.setError("Campo Vacío");

        }

    }

    public void validar_campos() {

        validar_Edittext(l_nombre, nombreneg.getText().toString());
        validar_Edittext(l_url, url_etx.getText().toString());
        validar_Edittext(l_desc, desc_neg.getText().toString());
        validar_Edittext(l_correo, correo.getText().toString());
        validar_Edittext(l_numero, numero.getText().toString());

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}