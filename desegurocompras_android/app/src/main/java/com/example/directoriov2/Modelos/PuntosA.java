package com.example.directoriov2.Modelos;

public class PuntosA {

    private String corte;
    private String detalles;

    public String getCorte() {
        return corte;
    }

    public void setCorte(String corte) {
        this.corte = corte;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public PuntosA(String corte, String detalles) {
        this.corte = corte;
        this.detalles = detalles;
    }
}
