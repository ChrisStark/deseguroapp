package com.example.directoriov2.Modelos;

public class VentasP {

    private String vpnombren;
    private String vpfecha;
    private String vpncomprador;
    private String vpdescart;
    private String vpcantart;
    private String vpprecioart;
    private String vptotalv;

    public String getVpnombren() {
        return vpnombren;
    }

    public void setVpnombren(String vpnombren) {
        this.vpnombren = vpnombren;
    }

    public String getVpfecha() {
        return vpfecha;
    }

    public void setVpfecha(String vpfecha) {
        this.vpfecha = vpfecha;
    }

    public String getVpncomprador() {
        return vpncomprador;
    }

    public void setVpncomprador(String vpncomprador) {
        this.vpncomprador = vpncomprador;
    }

    public String getVpdescart() {
        return vpdescart;
    }

    public void setVpdescart(String vpdescart) {
        this.vpdescart = vpdescart;
    }

    public String getVpcantart() {
        return vpcantart;
    }

    public void setVpcantart(String vpcantart) {
        this.vpcantart = vpcantart;
    }

    public String getVpprecioart() {
        return vpprecioart;
    }

    public void setVpprecioart(String vpprecioart) {
        this.vpprecioart = vpprecioart;
    }

    public String getVptotalv() {
        return vptotalv;
    }

    public void setVptotalv(String vptotalv) {
        this.vptotalv = vptotalv;
    }

    public VentasP(String vpnombren, String vpfecha, String vpncomprador, String vpdescart, String vpcantart, String vpprecioart, String vptotalv) {
        this.vpnombren = vpnombren;
        this.vpfecha = vpfecha;
        this.vpncomprador = vpncomprador;
        this.vpdescart = vpdescart;
        this.vpcantart = vpcantart;
        this.vpprecioart = vpprecioart;
        this.vptotalv = vptotalv;
    }
}