package com.example.directoriov2.Modelos.DetalleProducto;

import com.example.directoriov2.Modelos.Productos;

import java.util.ArrayList;

public class ProductoDetalle {


    private Productos producto;
    private ArrayList<Imagenes> imagenes;
    private Detalle detalle;

    public Productos getProducto() {
        return producto;
    }

    public void setProducto(Productos productos) {
        this.producto = productos;
    }

    public ArrayList<Imagenes> getImagenes() {
        return imagenes;
    }

    public void setImagenes(ArrayList<Imagenes> imagenes) {
        this.imagenes = imagenes;
    }

    public Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Detalle detalle) {
        this.detalle = detalle;
    }
}
