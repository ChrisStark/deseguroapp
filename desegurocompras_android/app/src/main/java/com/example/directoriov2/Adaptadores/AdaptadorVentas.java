package com.example.directoriov2.Adaptadores;

import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.directoriov2.Clases.DialogoDetallesVenta;
import com.example.directoriov2.Fragments.Ventas.VentasFragment;
import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.Modelos.Ventas;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class AdaptadorVentas extends RecyclerView.Adapter<AdaptadorVentas.ViewHolderVentas> {

    public interface OnItemClickListener {

        void onItemClick(int posicion);

    }

    private Context context;
    private ArrayList<Ventas> ventas;
    private AdaptadorVentas.OnItemClickListener listener;

    public AdaptadorVentas(Context context, ArrayList<Ventas> ventas, OnItemClickListener listener) {

        this.context = context;
        this.ventas = ventas;
        this.listener = listener;

    }

    @Override
    public ViewHolderVentas onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = 0;
        layout = R.layout.ventas_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderVentas(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolderVentas holder, int position) {

        holder.fechav.setText(ventas.get(position).getFechav());
        holder.compradorv.setText(ventas.get(position).getCompradorv());
        holder.totalv.setText(ventas.get(position).getTotalv());

        holder.detalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onItemClick(holder.getAdapterPosition());

            }
        });

    }

    @Override
    public int getItemCount() {
        return ventas.size();
    }

    public class ViewHolderVentas extends RecyclerView.ViewHolder {

        private TextView fechav;
        private TextView compradorv;
        private TextView totalv;
        private TextView detalle;

        public ViewHolderVentas(View itemView) {

            super(itemView);
            fechav = itemView.findViewById(R.id.v_fechav);
            compradorv = itemView.findViewById(R.id.v_compradorv);
            totalv = itemView.findViewById(R.id.v_totalv);
            detalle = itemView.findViewById(R.id.v_detallesv);

        }
    }
}
