package com.example.directoriov2.Modelos;

public class ModeloModificarNegocio {

    private String nombre;
    private String url;
    private String descripcion;
    private String correo;
    private String numero;

    public ModeloModificarNegocio(String nombre, String url, String descripcion, String correo, String numero) {
        this.nombre = nombre;
        this.url = url;
        this.descripcion = descripcion;
        this.correo = correo;
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
