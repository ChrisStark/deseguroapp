package com.example.directoriov2.Fragments.MiRed;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.directoriov2.Adaptadores.AdaptadorUplines;
import com.example.directoriov2.Clases.DialogoUplines;
import com.example.directoriov2.Modelos.Uplines;
import com.example.directoriov2.R;

import java.util.ArrayList;


public class UplinesFragment extends Fragment implements AdaptadorUplines.MyClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<Uplines> lista_uplines;
    RecyclerView recyclerView;
    AdaptadorUplines adaptadorUplines;

    View view;

    private OnFragmentInteractionListener mListener;

    public UplinesFragment() {
        // Required empty public constructor
    }

    public static UplinesFragment newInstance(String param1, String param2) {
        UplinesFragment fragment = new UplinesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_uplines, container, false);
        lista_uplines = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_uplines);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        llenar_lista();
        adaptadorUplines = new AdaptadorUplines(getContext(), lista_uplines);
        recyclerView.setAdapter(adaptadorUplines);
        adaptadorUplines.setOnItemClickListener(this);
        return view;
    }

    private void llenar_lista() {

        lista_uplines.add(new Uplines("5", "1902132", "Luis Ruiz Garrido"));
        lista_uplines.add(new Uplines("3", "1902133", "Francisco Javier Castillo Contreras"));
        lista_uplines.add(new Uplines("5", "1902134", "José Alfredo Jímenez"));
        lista_uplines.add(new Uplines("1", "1902135", "Cristopher Jairo Hernández Ramírez"));
        lista_uplines.add(new Uplines("1", "1902136", "Sergio Villeda Vargas"));
        lista_uplines.add(new Uplines("2", "1902137", "Rogelio Hernández Herrera"));
        lista_uplines.add(new Uplines("6", "1902138", "Miguel Ángel Sánchez Victoriano"));
        lista_uplines.add(new Uplines("1", "1902139", "Carolina Trejo Ugalde"));
        lista_uplines.add(new Uplines("1", "19021310", "Isabell Tenjhay"));
        lista_uplines.add(new Uplines("2", "19021311", "Diego Isaac Herrera Ledezma"));
        lista_uplines.add(new Uplines("9", "19021312", "Manuel Alejandro Reyes Amaya"));

        lista_uplines.add(new Uplines("5", "1902132", "Luis Ruiz Garrido"));
        lista_uplines.add(new Uplines("3", "1902133", "Francisco Javier Castillo Contreras"));
        lista_uplines.add(new Uplines("5", "1902134", "José Alfredo Jímenez"));
        lista_uplines.add(new Uplines("1", "1902135", "Cristopher Jairo Hernández Ramírez"));
        lista_uplines.add(new Uplines("1", "1902136", "Sergio Villeda Vargas"));
        lista_uplines.add(new Uplines("2", "1902137", "Rogelio Hernández Herrera"));
        lista_uplines.add(new Uplines("6", "1902138", "Miguel Ángel Sánchez Victoriano"));
        lista_uplines.add(new Uplines("1", "1902139", "Carolina Trejo Ugalde"));
        lista_uplines.add(new Uplines("1", "19021310", "Isabell Tenjhay"));
        lista_uplines.add(new Uplines("2", "19021311", "Diego Isaac Herrera Ledezma"));
        lista_uplines.add(new Uplines("9", "19021312", "Manuel Alejandro Reyes Amaya"));

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(int position, View v) {
        Toast.makeText(getActivity(), "item " + lista_uplines.get(position).getNompat(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDialog(int i) {
        DialogoUplines dialog = new DialogoUplines();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialog.show(ft, DialogoUplines.TAG);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}