package com.example.directoriov2.Clases;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.directoriov2.R;

public class DialogoDetallesVenta extends DialogFragment {

    public static String TAG = "DialogoDEtallesVenta";

    TextView cantidad;
    TextView descripcion;

    View view;

    public DialogoDetallesVenta() {


    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.dialogo_ventas_realizadas, container, false);
        setToolbar();
        inicializar();
        return view;

    }

    private void inicializar() {

        cantidad = view.findViewById(R.id.v_cantidad);
        descripcion = view.findViewById(R.id.v_desc);

    }

    private void setToolbar() {

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setTitle("Detalles de la Venta");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

}
