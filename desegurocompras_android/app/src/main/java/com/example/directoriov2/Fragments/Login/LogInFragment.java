package com.example.directoriov2.Fragments.Login;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Clases.Session;
import com.example.directoriov2.ListaActivity;
import com.example.directoriov2.Modelos.Login;
import com.example.directoriov2.Modelos.Usuario;
import com.example.directoriov2.R;
import com.example.directoriov2.serviciosAPI.APIServices;
import com.example.directoriov2.serviciosAPI.InstanciaRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private TextInputLayout textEmail;
    private TextInputEditText editEmail;
    private TextInputLayout textPassword;
    private TextInputEditText editPassword;
    private Button btn_login;
    private TextView tv_registra;
    private View view;
    private String token;
    private Session session;

    private OnFragmentInteractionListener mListener;

    public LogInFragment() {

    }

    public static LogInFragment newInstance(String param1, String param2) {
        LogInFragment fragment = new LogInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        session = new Session(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        controles_carg();

        return view;
    }

    private void controles_carg() {
        tv_registra = (TextView) view.findViewById(R.id.tv_registra);
        btn_login = (Button) view.findViewById(R.id.btn_login);

        //TextInput
        textEmail = view.findViewById(R.id.il_user);
        textPassword = view.findViewById(R.id.il_pass);
        editEmail =  view.findViewById(R.id.tiet_user);
        editPassword = view.findViewById(R.id.tiet_pass);

        tv_registra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistroFragment registroFragment = new RegistroFragment();
                FragmentManager manager =  getFragmentManager();
                manager.beginTransaction().replace(R.id.contenedorFragments, registroFragment, registroFragment.getTag())
                        .commit();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pass = editPassword.getText().toString();
                String email = editEmail.getText().toString();

                if (validateEmail() && validatePassword()) {
                    Login datosLogin =  new Login(email, pass);
                    APIServices apiServices = InstanciaRetrofit.getRetrofitInstanceLogin().create(APIServices.class);
                    Call<Usuario> call = apiServices.loginUsuario(datosLogin);
                    callbacklogin(call,email);

                }else {
                    Toast.makeText(getActivity(), "Campos Vacíos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void callbacklogin(Call<Usuario> call, final String email) {
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if (response.isSuccessful()) {
                    token = response.body().getAuth_token();
                    session.setLogeado(true,token,email);
                    Toast.makeText(getActivity(), "el token es "+token, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), ListaActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Toast.makeText(getActivity(), "Datos Incorrectos, Verifique", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(getActivity(), "Error ", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public boolean validateEmail() {
        String emailInput = textEmail.getEditText().getText().toString().trim();

        if(emailInput.isEmpty()) {
            textEmail.setError("Debe llenar el campo para iniciar sesión.");
            return false;
        }else{
            textEmail.setError(null);
            return true;
        }
    }

    public boolean validatePassword() {

        String password = textPassword.getEditText().getText().toString().trim();

        if (password.isEmpty()) {
        textPassword.setError("La contraseña debe contener de 6 a 15 caracteres.");
        return false;
        }else if (password.length()>15) {
            textPassword.setError("La contraseña no debe tener más de 15 caracteres.");
            return false;
        }else {
            textPassword.setError(null);
            return true;
        }

    }

    public void Login (View view) {

        if (!validateEmail()||!validatePassword()){
            return;
        }

        String input = "Correo: "+textEmail.getEditText().getText().toString();
        input += "\n";
        input = "Contraseña: "+textPassword.getEditText().getText().toString();

        Toast.makeText(getActivity(), input, Toast.LENGTH_SHORT).show();


    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
