package com.example.directoriov2.Fragments.Paquetes;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.directoriov2.Adaptadores.AdaptadorPaquetes;
import com.example.directoriov2.Modelos.Paquetes;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class PaquetesFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<Paquetes> lista_paquetes;
    RecyclerView recyclerView;
    AdaptadorPaquetes adaptadorPaquetes;

    View view;

    private OnFragmentInteractionListener mListener;

    public PaquetesFragment() {
        // Required empty public constructor
    }

    public static PaquetesFragment newInstance(String param1, String param2) {
        PaquetesFragment fragment = new PaquetesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_paquetes, container, false);
        lista_paquetes = new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_paquetes);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        llenar_lista();
        adaptadorPaquetes = new AdaptadorPaquetes(getContext(), lista_paquetes);
        recyclerView.setAdapter(adaptadorPaquetes);
        return view;
    }

    private void llenar_lista() {

        lista_paquetes.add(new Paquetes("Oro Inicial","50", "Negocios","(1)", "150.00"));
        lista_paquetes.add(new Paquetes("100 Puntos","100", " "," ", "120.00"));
        lista_paquetes.add(new Paquetes("Registrar Negocio","100", " "," ", "664.00"));
        lista_paquetes.add(new Paquetes("Bronce","200", "Comisiones","(3)", "764.00"));
        lista_paquetes.add(new Paquetes("Oro","300", "Comisiones","(6)", "864.00"));
        lista_paquetes.add(new Paquetes("Diamante","500", "Comisiones","(9)", "999.00"));

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
