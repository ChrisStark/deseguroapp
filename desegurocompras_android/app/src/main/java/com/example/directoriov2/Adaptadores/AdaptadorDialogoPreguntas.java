package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.directoriov2.Modelos.ModelDialogoPreguntas;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class AdaptadorDialogoPreguntas extends RecyclerView.Adapter<AdaptadorDialogoPreguntas.ViewHolderPreguntas> {
    private ArrayList<ModelDialogoPreguntas> listaPreguntas;
    private Context context;
    private AdaptadorDialogoPreguntas.MyClickListener sClickListener;

    public AdaptadorDialogoPreguntas(Context context, ArrayList<ModelDialogoPreguntas> listaPreguntas) {
        this.listaPreguntas = listaPreguntas;
        this.context = context;
    }

    @Override
    public ViewHolderPreguntas onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = 0;
        layout = R.layout.dialogo_preguntas_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderPreguntas(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderPreguntas holder, int position) {
        holder.pregunta.setText(listaPreguntas.get(position).getPregunta());
        if (listaPreguntas.get(position).getRespuesa() == null) {
            holder.RLrespuesta.setVisibility(View.GONE);
        } else {
            holder.RLrespuesta.setVisibility(View.VISIBLE);
            holder.respuesta.setText(listaPreguntas.get(position).getRespuesa());
            holder.fecha.setText(listaPreguntas.get(position).getFecha());
        }
    }


    @Override
    public int getItemCount() {
        return listaPreguntas.size();
    }

    public class ViewHolderPreguntas extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView pregunta, respuesta, fecha;
        RelativeLayout RLpregunta, RLrespuesta, RLfecha;

        public ViewHolderPreguntas(View itemView) {
            super(itemView);
            RLpregunta = itemView.findViewById(R.id.RLpregunta);
            RLrespuesta = itemView.findViewById(R.id.RLrespuesta);
            RLfecha = itemView.findViewById(R.id.RLfecha);
            pregunta = itemView.findViewById(R.id.pregunta);
            respuesta = itemView.findViewById(R.id.respuesta);
            fecha = itemView.findViewById(R.id.fecha);
            //   itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            sClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
        //void addItemClick()
    }

    public void setOnItemClickListener(AdaptadorDialogoPreguntas.MyClickListener myClickListener) {
        this.sClickListener = myClickListener;

    }

}
