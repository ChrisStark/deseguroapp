package com.example.directoriov2.Fragments.Ventas;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.directoriov2.Adaptadores.AdaptadorVentas;
import com.example.directoriov2.Clases.DialogoDetallesVenta;
import com.example.directoriov2.Modelos.Ventas;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class VentasFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<Ventas> lista_ventas;
    RecyclerView recyclerView;
    AdaptadorVentas adaptadorVentas;

    View view;

    private OnFragmentInteractionListener mListener;

    public VentasFragment() {
        // Required empty public constructor
    }

    public static VentasFragment newInstance(String param1, String param2) {
        VentasFragment fragment = new VentasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ventas, container, false);
        lista_ventas =  new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_ventas);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        llenar_lista();
        adaptadorVentas = new AdaptadorVentas(getContext(), lista_ventas, new AdaptadorVentas.OnItemClickListener() {
            @Override
            public void onItemClick(int posicion) {
                FragmentManager fragmentManager = getFragmentManager();
                DialogoDetallesVenta dialogoDetallesVenta = new DialogoDetallesVenta();
                dialogoDetallesVenta.show(fragmentManager,DialogoDetallesVenta.TAG);
            }
        });
        recyclerView.setAdapter(adaptadorVentas);
        return view;
    }


    private void llenar_lista() {

        lista_ventas.add(new Ventas("07/03/2019 16:20:25 -0600","Ethan Navarrete", "499.00"));
        lista_ventas.add(new Ventas("06/03/2019 20:43:56 -0600","Diego Herrera", "399.00"));
        lista_ventas.add(new Ventas("02/03/2019 17:20:35 -0600","Christian Ramírez", "200.00"));
        lista_ventas.add(new Ventas("01/03/2019 18:18:58 -0600","Rogelio Hernández", "250.00"));
        lista_ventas.add(new Ventas("25/02/2019 09:05:20 -0600","Miguel Sánchez", "300000.00"));
        lista_ventas.add(new Ventas("22/02/2019 13:05:19 -0600","Osvaldo Lizardi", "799.00"));
        lista_ventas.add(new Ventas("21/02/2019 10:25:19 -0600","Alejandro Reyes", "850.00"));
        lista_ventas.add(new Ventas("16/02/2019 11:50:32 -0600","José Vega", "300.00"));
        lista_ventas.add(new Ventas("10/02/2019 23:59:59 -0600","Ivanovich Rechi", "699.00"));
        lista_ventas.add(new Ventas("09/02/2019 16:02:55 -0600" ,"Rodrigo Hernández", "999.00"));
        lista_ventas.add(new Ventas("03/02/2019 22:20:25 -0600","Daniel Perez", "15325.00"));

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

}
