package com.example.directoriov2.Modelos;

public class Compras {

    private String nombreneg;
    private String fechac;
    private String nomart;
    private String cantidadart;
    private String precioart;
    private String totalcomp;

    public String getNombreneg() {
        return nombreneg;
    }

    public void setNombreneg(String nombreneg) {
        this.nombreneg = nombreneg;
    }

    public String getFechac() {
        return fechac;
    }

    public void setFechac(String fechac) {
        this.fechac = fechac;
    }

    public String getNomart() {
        return nomart;
    }

    public void setNomart(String nomart) {
        this.nomart = nomart;
    }

    public String getCantidadart() {
        return cantidadart;
    }

    public void setCantidadart(String cantidadart) {
        this.cantidadart = cantidadart;
    }

    public String getPrecioart() {
        return precioart;
    }

    public void setPrecioart(String precioart) {
        this.precioart = precioart;
    }

    public String getTotalcomp() {
        return totalcomp;
    }

    public void setTotalcomp(String totalcomp) {
        this.totalcomp = totalcomp;
    }

    public Compras(String nombreneg, String fechac, String nomart, String cantidadart, String precioart, String totalcomp) {
        this.nombreneg = nombreneg;
        this.fechac = fechac;
        this.nomart = nomart;
        this.cantidadart = cantidadart;
        this.precioart = precioart;
        this.totalcomp = totalcomp;
    }
}
