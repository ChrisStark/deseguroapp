package com.example.directoriov2.Fragments.YidiPuntos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.AdaptadorPuntosP;
import com.example.directoriov2.Modelos.PuntosP;
import com.example.directoriov2.R;

import java.util.ArrayList;


public class PuntosPendFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<PuntosP> lista_puntosp;
    RecyclerView recyclerView;
    AdaptadorPuntosP adaptadorPuntosP;

    View view;

    private OnFragmentInteractionListener mListener;

    public PuntosPendFragment() {
        // Required empty public constructor
    }

    public static PuntosPendFragment newInstance(String param1, String param2) {
        PuntosPendFragment fragment = new PuntosPendFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_puntos_pend, container, false);
        lista_puntosp = new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_puntospend);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        llenar_lista();
        adaptadorPuntosP =  new AdaptadorPuntosP(getContext(), lista_puntosp);
        recyclerView.setAdapter(adaptadorPuntosP);
        return view;
    }

    private void llenar_lista() {

        lista_puntosp.add(new PuntosP("V","2", "19/02/2019", "Rafael Lozano Rueda", "Comisión por venta de artículo", "15.20","75.00%","11.40"));
        lista_puntosp.add(new PuntosP("V","4", "22/02/2019", "Luis Ruiz Garrido", "Comisión por venta de artículo", "35.00","75.00%","26.25"));
        lista_puntosp.add(new PuntosP("C","1", "26/02/2019", "Francisco Javier Castillo Contreras", "Comisión por compra de artículo", "150.00","20.00%","30.00"));
        lista_puntosp.add(new PuntosP("C","1", "26/02/2019", "Francisco Javier Castillo Contreras", "Comisión por compra de artículo", "150.00","20.00%","30.00"));
        lista_puntosp.add(new PuntosP("C","1", "26/02/2019", "Francisco Javier Castillo Contreras", "Comisión por compra de artículo", "150.00","20.00%","30.00"));
        lista_puntosp.add(new PuntosP("C","1", "26/02/2019", "Francisco Javier Castillo Contreras", "Comisión por compra de artículo", "150.00","20.00%","30.00"));
        lista_puntosp.add(new PuntosP("C","1", "26/02/2019", "Francisco Javier Castillo Contreras", "Comisión por compra de artículo", "150.00","20.00%","30.00"));
        lista_puntosp.add(new PuntosP("C","1", "26/02/2019", "Francisco Javier Castillo Contreras", "Comisión por venta de artículo", "200.00","20.00%","40.00"));


    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
