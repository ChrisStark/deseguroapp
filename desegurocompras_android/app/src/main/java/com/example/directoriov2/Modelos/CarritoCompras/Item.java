package com.example.directoriov2.Modelos.CarritoCompras;

public class Item {
    private String puntos_importe;
    private String puntos_unitario;
    private String putnos_comisionables;
    private String unitario;
    private String importe;
    private String descripcion;
    private int cantidad;

    public String getPuntos_importe() {
        return puntos_importe;
    }

    public void setPuntos_importe(String puntos_importe) {
        this.puntos_importe = puntos_importe;
    }

    public String getPuntos_unitario() {
        return puntos_unitario;
    }

    public void setPuntos_unitario(String puntos_unitario) {
        this.puntos_unitario = puntos_unitario;
    }

    public String getPutnos_comisionables() {
        return putnos_comisionables;
    }

    public void setPutnos_comisionables(String putnos_comisionables) {
        this.putnos_comisionables = putnos_comisionables;
    }

    public String getUnitario() {
        return unitario;
    }

    public void setUnitario(String unitario) {
        this.unitario = unitario;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}