package com.example.directoriov2.Fragments.Compras;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.ViewPagerFragmentAdapter;
import com.example.directoriov2.R;

public class ContenedorComprasFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private View view;
    ViewPager viewPager;
    TabLayout tabLayout;
    ViewPagerFragmentAdapter viewPagerFragmentAdapter;

    public ContenedorComprasFragment() {
    }

    public static ContenedorComprasFragment newInstance(String param1, String param2) {
        ContenedorComprasFragment fragment = new ContenedorComprasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contenedor_compras,container,false);

        tabLayout = view.findViewById(R.id.tabLayout_compras);
        viewPager = view.findViewById(R.id.viewPagerCompras);
        viewPagerFragmentAdapter = new ViewPagerFragmentAdapter((getFragmentManager()));
        viewPagerFragmentAdapter.addFragment(new ComprasFragment(),"Compras");
        viewPagerFragmentAdapter.addFragment(new ComprasPendFragment(),"Compras Pendientes");
        viewPager.setAdapter(viewPagerFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(1).select();
        return view;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
