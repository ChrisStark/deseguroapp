package com.example.directoriov2.Modelos;

public class Usuario {
    private String email;
    private String password;
    private String auth_token;

    public Usuario(String email, String password, String auth_token) {
        this.email = email;
        this.password = password;
        this.auth_token = auth_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}