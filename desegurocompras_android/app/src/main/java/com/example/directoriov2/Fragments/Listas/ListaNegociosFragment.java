package com.example.directoriov2.Fragments.Listas;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.AdaptadorListaNegocios;
import com.example.directoriov2.DescripcionActivity;
import com.example.directoriov2.MainActivity;
import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.R;

import java.io.Serializable;
import java.util.ArrayList;

public class ListaNegociosFragment extends Fragment {

    View view;
    ArrayList<Negocio> listaNegocios;
    RecyclerView recyclerView;
    AdaptadorListaNegocios adaptador;
    private OnFragmentInteractionListener mListener;
    public ListaNegociosFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lista_negocios,container,false);
        init();
        return view;
    }
    private void init() {
        listaNegocios = new ArrayList<>();
        cargarRecycler();
    }


    private void cargarRecycler() {
        RecyclerView.LayoutManager layoutManager;
        recyclerView = view.findViewById(R.id.RV_lista_negocios);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        llenarNegocios(); //respuesta del API


        adaptador = new AdaptadorListaNegocios(getContext(),listaNegocios, new AdaptadorListaNegocios.OnItemClickListener() {
            @Override
            public void onItemClick(Negocio item) {
                Intent intent = new Intent(getActivity(), DescripcionActivity.class);
                intent.putExtra("objeto", item);
                intent.putExtra("tag",DescripcionActivity.TAG_LISTA_NEGOCIOS);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adaptador);

    }


    public void llenarNegocios() {
        String url ="http://oficina-beta.yatvii.com/system/56/negocio_1/logo/small/logo1.jpg?1553104817";
        listaNegocios.add(new Negocio("prendas",url,""));
        listaNegocios.add(new Negocio("prendas",url,""));
        listaNegocios.add(new Negocio("prendas",url,""));
        listaNegocios.add(new Negocio("prendas",url,""));
        listaNegocios.add(new Negocio("prendas",url,""));
        listaNegocios.add(new Negocio("prendas",url,""));

    }

    public String formatoImagenTiny(String imagen) {
        return imagen.replace("/original/", "/tiny/");
    }

    public String formatoImagenSmall(String imagen) {
        return imagen.replace("/original/", "/small/");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        SearchView buscador;
        inflater.inflate(R.menu.menu_search, menu);
        buscador = (SearchView) menu.findItem(R.id.buscador).getActionView();
        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adaptador.getFilter().filter(newText);
                return true;
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.carrito) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra("opcion", 1);
            startActivity(intent);

//            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}