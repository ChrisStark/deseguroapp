package com.example.directoriov2.Modelos.CarritoCompras;

public class Compra {
    private int id;
    private String puntos_comisionables;
    private String total;
    private String updated_at; //fecha en la que se grego al carrito de compra
    private String created_at;
    private int cerrada;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPuntos_comisionables() {
        return puntos_comisionables;
    }

    public void setPuntos_comisionables(String puntos_comisionables) {
        this.puntos_comisionables = puntos_comisionables;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int isCerrada() {
        return cerrada;
    }

    public void setCerrada(int cerrada) {
        this.cerrada = cerrada;
    }
}