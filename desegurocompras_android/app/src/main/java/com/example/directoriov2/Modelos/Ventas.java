package com.example.directoriov2.Modelos;

public class Ventas {

    private String fechav;
    private String compradorv;
    private String totalv;

    public String getFechav() {
        return fechav;
    }

    public void setFechav(String fechav) {
        this.fechav = fechav;
    }

    public String getCompradorv() {
        return compradorv;
    }

    public void setCompradorv(String compradorv) {
        this.compradorv = compradorv;
    }

    public String getTotalv() {
        return totalv;
    }

    public void setTotalv(String totalv) {
        this.totalv = totalv;
    }

    public Ventas(String fechav, String compradorv, String totalv) {

        this.fechav = fechav;
        this.compradorv = compradorv;
        this.totalv = totalv;

    }
}
