package com.example.directoriov2.Modelos;


public class Publicaciones {
    private int id;
    private String nombre;
    private String precio;
    private String image_url;
    private String precio_descuento;

    public Publicaciones(int id, String nombre, String precio, String image_url, String precio_descuento) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.image_url = image_url;
        this.precio_descuento = precio_descuento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getPrecio_descuento() {
        return precio_descuento;
    }

    public void setPrecio_descuento(String precio_descuento) {
        this.precio_descuento = precio_descuento;
    }
}

