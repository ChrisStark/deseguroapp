package com.example.directoriov2.Modelos;

public class ModelDialogoPreguntas {

    private String pregunta;
    private String respuesa;
    private String fecha;

    public ModelDialogoPreguntas(String pregunta, String respuesa, String fecha) {
        this.pregunta = pregunta;
        this.respuesa = respuesa;
        this.fecha = fecha;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesa() {
        return respuesa;
    }

    public void setRespuesa(String respuesa) {
        this.respuesa = respuesa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}

