package com.example.directoriov2.Modelos.CarritoCompras;

import java.io.Serializable;

public class Carrito implements Serializable {

    private int Id;
    private String Imagen;
    private String Titulo;
    private String updated_at;
    private int Cantidad;
    private String Precio;
    private String total;
    private String negocio;

    public Carrito(String imagen, String titulo, String updated_at, int cantidad, String precio, String total,String negocio) {
        this.Imagen = imagen;
        this.Titulo = titulo;
        this.updated_at = updated_at;
        this.Cantidad = cantidad;
        this.Precio = precio;
        this.total = total;
        this.negocio = negocio;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String imagen) {
        Imagen = imagen;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getFecha() {
        return updated_at;
    }

    public void setFecha(String updated_at) {
        updated_at = updated_at;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        this.Cantidad = cantidad;
    }

    public String getPrecio() {
        return Precio;
    }
    public void setPrecio(String precio) {

        this.Precio = precio;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
