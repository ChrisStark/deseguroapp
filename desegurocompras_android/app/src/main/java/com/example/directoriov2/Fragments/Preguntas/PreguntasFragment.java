package com.example.directoriov2.Fragments.Preguntas;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.AdaptadorPreguntas;
import com.example.directoriov2.Modelos.Preguntas;
import com.example.directoriov2.R;

import java.util.ArrayList;


public class PreguntasFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    View view;
    AdaptadorPreguntas adaptador;
    RecyclerView recycler;
    ArrayList<Preguntas> listaPreguntas;

    public PreguntasFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_preguntas, container, false);
        init();
        return view;
    }

    private void init() {
        listaPreguntas = new ArrayList<>();
        recycler = view.findViewById(R.id.RVpreguntas);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        llenarPregunas();
        adaptador = new AdaptadorPreguntas(getContext(),listaPreguntas);
        recycler.setAdapter(adaptador);

    }

    private void llenarPregunas() {
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));
        listaPreguntas.add(new Preguntas("pregunta?", "tu pregunta aun no ha sido contestada", "laptop"));

    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
