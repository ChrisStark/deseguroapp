package com.example.directoriov2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Clases.Session;
import com.example.directoriov2.Clases.Ubicacion;
import com.example.directoriov2.Fragments.Compras.ComprasFragment;
import com.example.directoriov2.Fragments.Compras.ComprasPendFragment;
import com.example.directoriov2.Fragments.Compras.ContenedorComprasFragment;
import com.example.directoriov2.Fragments.Login.LogInFragment;
import com.example.directoriov2.Fragments.Login.RegistroFragment;
import com.example.directoriov2.Fragments.MiRed.ContenedorRedFragment;
import com.example.directoriov2.Fragments.MiRed.DownlinesFragment;
import com.example.directoriov2.Fragments.MiRed.UplinesFragment;
import com.example.directoriov2.Fragments.Negocios.NegociosFragment;
import com.example.directoriov2.Fragments.Paquetes.PaquetesFragment;
import com.example.directoriov2.Fragments.Perfil.MisDatosFragment;
import com.example.directoriov2.Fragments.Preguntas.PreguntasFragment;
import com.example.directoriov2.Fragments.Ventas.ContenedorVentasFragment;
import com.example.directoriov2.Fragments.Ventas.VentasFragment;
import com.example.directoriov2.Fragments.Ventas.VentasPendFragment;
import com.example.directoriov2.Fragments.YidiPuntos.ContenedorPuntosFragment;
import com.example.directoriov2.Fragments.YidiPuntos.PuntosAcomFragment;
import com.example.directoriov2.Fragments.YidiPuntos.PuntosPendFragment;


public class MainActivity extends AppCompatActivity
        implements LogInFragment.OnFragmentInteractionListener, RegistroFragment.OnFragmentInteractionListener,
        MisDatosFragment.OnFragmentInteractionListener, ContenedorComprasFragment.OnFragmentInteractionListener,
        ContenedorVentasFragment.OnFragmentInteractionListener, ContenedorPuntosFragment.OnFragmentInteractionListener,
        ContenedorRedFragment.OnFragmentInteractionListener, NegociosFragment.OnFragmentInteractionListener,
        PreguntasFragment.OnFragmentInteractionListener, PaquetesFragment.OnFragmentInteractionListener,
        ComprasFragment.OnFragmentInteractionListener, ComprasPendFragment.OnFragmentInteractionListener,
        VentasFragment.OnFragmentInteractionListener, VentasPendFragment.OnFragmentInteractionListener,
        DownlinesFragment.OnFragmentInteractionListener, UplinesFragment.OnFragmentInteractionListener,
        PuntosAcomFragment.OnFragmentInteractionListener, PuntosPendFragment.OnFragmentInteractionListener {

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    Session session;
    NavigationView navigationView;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session= new Session(this);
        if (getIntent().getExtras() != null) {
            id = getIntent().getExtras().getInt("opcion");
        }
        Toast.makeText(this, ""+id, Toast.LENGTH_SHORT).show();

        setToolbar_Drawer();
        if (session.getLogeado()) {
            View view = navigationView.getHeaderView(0);
            CardView cardView = view.findViewById(R.id.CVlogin);
            TextView tvEmail = (TextView) view.findViewById(R.id.email_user);
            String email = session.getEmail();
            tvEmail.setText(email);
            cardView.setVisibility(View.GONE);
            Toast.makeText(this, "usuario logeado" + email, Toast.LENGTH_LONG).show();
        }
        Fragment mifragment = new LogInFragment();
        switch (id){
            case R.id.op_user:
                mifragment = new MisDatosFragment();
                break;
            case R.id.op_compras:
                mifragment = new ContenedorComprasFragment();
                break;
            case R.id.op_ventas:
                mifragment = new ContenedorVentasFragment();
                break;
            case R.id.op_negocios:
                mifragment = new NegociosFragment();
                break;
            case R.id.op_preguntas:
                mifragment = new PreguntasFragment();
                break;
            case R.id.op_red:
                mifragment = new ContenedorRedFragment();
                break;
            case R.id.op_puntos:
                mifragment = new ContenedorPuntosFragment();
                break;
            case R.id.op_paquetes:
                mifragment = new PaquetesFragment();
                break;
            case 1:
                mifragment = new ContenedorComprasFragment();
        }
        getSupportFragmentManager().beginTransaction().add(R.id.contenedorFragments, mifragment).commit();
    }

    private void setToolbar_Drawer() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayoutMain);
        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawerLayout.addDrawerListener(toogle);
        toogle.syncState();

        navigationView = (NavigationView) findViewById(R.id.navViewMain);
        if (navigationView != null) {
            setupNavigationDrawerContent(navigationView);
        }
        setupNavigationDrawerContent(navigationView);
    }


    private void setupNavigationDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        Fragment mifragment = new LogInFragment();
                        boolean fragmentSelect = false;
                        switch (menuItem.getItemId()) {
                            case R.id.ubicacion:
                                obtenerUbicacion();
                                break;
                            case R.id.op_user:
                                mifragment = new MisDatosFragment();
                                fragmentSelect = true;
                                break;
                            case R.id.op_compras:
                                mifragment = new ContenedorComprasFragment();
                                fragmentSelect = true;
                                break;
                            case R.id.op_ventas:
                                mifragment = new ContenedorVentasFragment();
                                fragmentSelect = true;
                                break;
                            case R.id.op_negocios:
                                mifragment = new NegociosFragment();
                                fragmentSelect = true;
                                break;
                            case R.id.op_preguntas:
                                mifragment = new PreguntasFragment();
                                fragmentSelect = true;
                                break;
                            case R.id.op_red:
                                mifragment = new ContenedorRedFragment();
                                fragmentSelect = true;

                                break;
                            case R.id.op_puntos:
                                mifragment = new ContenedorPuntosFragment();
                                fragmentSelect = true;

                                break;
                            case R.id.op_paquetes:
                                mifragment = new PaquetesFragment();
                                fragmentSelect = true;

                                break;
                            case R.id.salir:
                                logout();
                                break;

                        }

                        if(fragmentSelect){

                                FragmentManager fragmentManager = getSupportFragmentManager();
                                FragmentTransaction ft = fragmentManager.beginTransaction();
                                ft.replace(R.id.contenedorFragments,mifragment);
                                ft.commit();
                              //  getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments, mifragment).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);

                        }

                        return true;
                    }
                });
    }
    public void logout(){
        session.setLogeado(false,"","");
        Intent intent = new Intent(this,ListaActivity.class);
        startActivity(intent);
        finish();
    }


    public void onClick(View view) {
        LogInFragment logInFragment = new LogInFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments, logInFragment).commit();

    }
    private void obtenerUbicacion() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        if (ActivityCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(MainActivity.this, "permiso denegado", Toast.LENGTH_SHORT).show();
        } else {
            Ubicacion ubicacion = new Ubicacion(getApplicationContext());
            ubicacion.getLocation();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}