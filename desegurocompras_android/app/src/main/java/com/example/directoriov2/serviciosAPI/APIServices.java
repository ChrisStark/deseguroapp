package com.example.directoriov2.serviciosAPI;

import com.example.directoriov2.Modelos.CarritoCompras.ResponseCarrito;
import com.example.directoriov2.Modelos.ComprasPendientes;
import com.example.directoriov2.Modelos.Login;
import com.example.directoriov2.Modelos.PerfilUsuario;
import com.example.directoriov2.Modelos.DetalleProducto.ProductoDetalle;
import com.example.directoriov2.Modelos.Productos;
import com.example.directoriov2.Modelos.Usuario;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIServices {
   // antigua url
//    @GET("ds/publicaciones")
//    Call<ArrayList<Productos>> getProductos();
//
//    @GET("ds/publicacion/{id}")
//    Call<ProductoDetalle> getProductoDetalle(@Path("id") int id);
//nueva url

    @GET("ds/publicaciones")
    Call<ArrayList<Productos>> getProductos();

    @GET("subdominios/publicacion/{subdominio}/{id}")
    Call<ProductoDetalle> getProductoDetalle(@Path("id") int id,@Path("subdominio") String subdominio);

    @POST("authenticate") //sign_in
    Call<Usuario> loginUsuario(@Body Login login);

    @FormUrlEncoded
    @POST("oficina/perfil")
    Call<PerfilUsuario> getPerfilUsuario(@Header("Authorization")String token, @Field("email") String email);

    @FormUrlEncoded
    @POST("oficina/compras") //entrar a una direccion de oficina virtual con el token
    Call<ComprasPendientes> getCompras(@Header("Authorization") String token, @Field("email") String email);

    @FormUrlEncoded
    @POST("oficina/compras/carrito") //entrar a una direccion de oficina virtual con el token
    Call<ArrayList<ResponseCarrito>> getCarrito(@Header("Authorization") String token, @Field("email") String email);

}
