package com.example.directoriov2.Fragments.Negocios;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.AdaptadorNegocios;
import com.example.directoriov2.DescripcionActivity;
import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class NegociosFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<Negocio> lista_negocios;
    RecyclerView recyclerView;
    AdaptadorNegocios adaptadorNegocios;

    View view;

    private OnFragmentInteractionListener mListener;

    public NegociosFragment() {
        // Required empty public constructor
    }

    public static NegociosFragment newInstance(String param1, String param2) {
        NegociosFragment fragment = new NegociosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_negocios, container, false);
        lista_negocios = new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_negocios);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        llenar_lista();
        adaptadorNegocios = new AdaptadorNegocios(getContext(), lista_negocios, new AdaptadorNegocios.OnItemClickListener() {
            @Override
            public void onItemClick(Negocio item) {
                Intent intent = new Intent(getActivity(), DescripcionActivity.class);
                intent.putExtra("objeto", item);
                intent.putExtra("tag",DescripcionActivity.TAG_LISTA_NEGOCIOS_USUARIO);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adaptadorNegocios);
        return view;
    }

    public void llenar_lista() {
        String url ="http://oficina-beta.yatvii.com/system/56/negocio_1/logo/small/logo1.jpg?1553104817";
        lista_negocios.add(new Negocio("Autos", url, "se venden carros"));
        lista_negocios.add(new Negocio("Autos", url, "se venden carros"));
        lista_negocios.add(new Negocio("Autos", url, "se venden carros"));

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
