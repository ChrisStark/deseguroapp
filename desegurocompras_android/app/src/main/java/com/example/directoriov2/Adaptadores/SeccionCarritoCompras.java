package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.directoriov2.Fragments.Compras.ComprasPendFragment;
import com.example.directoriov2.Modelos.CarritoCompras.Carrito;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class SeccionCarritoCompras extends StatelessSection {
    private ArrayList<Carrito> carrito;
    private Context context;
    private String titulo;
    private MyClickListener sClickListener;
    final String TAG;

    public SeccionCarritoCompras(Context context,String tag, ArrayList<Carrito> carrito, String titulo, MyClickListener listener) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.carrito_row_items)
                .headerResourceId(R.layout.carrito_header_items)
                .build());
        this.context = context;
        this.carrito = carrito;
        this.titulo = titulo;
        this.TAG = tag;
        this.sClickListener = listener;
    }

    @Override
    public int getContentItemsTotal() {
        return carrito.size(); // number of items of this section
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        // return a custom instance of ViewHolder for the items of this section
        return new MyItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MyItemViewHolder itemHolder = (MyItemViewHolder) holder;
        final Carrito c = carrito.get(position);
        Picasso.get().
                load(carrito.get(position).getImagen())
                .fit()
                .into(itemHolder.imagen);
        itemHolder.nombre.setText(carrito.get(position).getTitulo());
        itemHolder.cantidad.setText(String.valueOf(carrito.get(position).getCantidad()));
        itemHolder.precio.setText(carrito.get(position).getPrecio());
        itemHolder.total.setText(carrito.get(position).getTotal());
        itemHolder.fecha.setText(carrito.get(position).getUpdated_at());
        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sClickListener.onItemClick(c);
            }
        });

        itemHolder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(context, itemHolder.options, Gravity.RIGHT);
                popup.inflate(R.menu.options);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.eliminar:
                                sClickListener.onDeleteClick(itemHolder.getAdapterPosition());
                                break;
                            case R.id.comprar:
                                sClickListener.onComprarClick(itemHolder.getAdapterPosition());
                                Toast.makeText(context, "comprar", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return false;
                    }
                });
                popup.show();

            }
        });
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
        headerHolder.titulo.setText(titulo);
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.sClickListener = myClickListener;
    }

    public interface MyClickListener {
        void onItemClick(Carrito carrito);
        void onDeleteClick(int position);
        void onComprarClick(int position);
       // void onComprarTodoClick(int position);
    }

    public class MyItemViewHolder extends RecyclerView.ViewHolder {
        final TextView fecha, nombre, precio, total, cantidad, options;
        final ImageView imagen;

        public MyItemViewHolder(View itemView) {
            super(itemView);
            options = itemView.findViewById(R.id.textViewOptions);
            imagen = itemView.findViewById(R.id.imagen);
            fecha = itemView.findViewById(R.id.fecha);
            nombre = itemView.findViewById(R.id.nombre);
            precio = itemView.findViewById(R.id.precio);
            total = itemView.findViewById(R.id.total);
            cantidad = itemView.findViewById(R.id.cantidad);
            //itemView.setOnClickListener(this);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private final TextView titulo;

        HeaderViewHolder(View view) {
            super(view);

            titulo = view.findViewById(R.id.nombre_negocio);
        }
    }
}
