package com.example.directoriov2.Fragments.Ventas;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.directoriov2.Adaptadores.AdaptadorVentasP;
import com.example.directoriov2.Modelos.VentasP;
import com.example.directoriov2.R;

import java.util.ArrayList;


public class VentasPendFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<VentasP> lista_ventasp;
    RecyclerView recyclerView;
    AdaptadorVentasP adaptadorVentasP;

    View view;

    private OnFragmentInteractionListener mListener;

    public VentasPendFragment() {
        // Required empty public constructor
    }

    public static VentasPendFragment newInstance(String param1, String param2) {
        VentasPendFragment fragment = new VentasPendFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ventas_pend, container, false);
        lista_ventasp = new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_ventasp);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        llenar_lsita();
        adaptadorVentasP = new AdaptadorVentasP(getContext(), lista_ventasp);
        recyclerView.setAdapter(adaptadorVentasP);
        return view;

    }

    private void llenar_lsita() {

        lista_ventasp.add(new VentasP("Autos", "11/03/2019 10:00:25 -0600", "Miguel Sánchez", "Mitsubishi Montero Limited Piel Qc 7 Pasajeros 4x4 At", "1", "159900.00", "159900.00"));
        lista_ventasp.add(new VentasP("Inmuebles", "08/03/2019 17:10:65 -0600", "Elia Francisco", "Av. Fresno 5, Bosques De San Juan, San Juan Del Río, Querétaro", "1", "1110000.00", "1110000.00"));
        lista_ventasp.add(new VentasP("Tecnología", "03/03/2019 16:35:52 -0600", "Daniel Pérez", "Xiaomi Redmi Note 6 Pro 64gb 4gb Ram 4g Lte Version Global", "1", "4900.00", "4900.00"));
        lista_ventasp.add(new VentasP("Tecnología", "03/03/2019 12:38:59 -0600", "José Vega", "Laptop Lenovo 330s Intel Ci5 8250u 1tb 16gb Optane 4gb Slim", "1", "11000.00", "11000.00"));
        lista_ventasp.add(new VentasP("Herramientas e Industrias", "01/03/2019 16:10:17 -0600", "Ethan Navarrete", "Taladro Black and Decker 6 en 1 Matrix 20v Tool Combo", "1", "3460.00", "3460.00"));
        lista_ventasp.add(new VentasP("Tecnología", "01/03/2019 12:19:22 -0600", "Rogelio Hernández", "Audífonos Bluetooth V8 Voyager Legend + Audífono Auxiliar", "1", "220.00", "220.00"));
        lista_ventasp.add(new VentasP("Muebles de Oficina", "27/02/2019 13:57:35 -0600", "Osvaldo Lizardi", "Silla Oficina Ejecutiva Base Metálica Cromada Respaldo Malla", "1", "1250.00", "1250.00"));
        lista_ventasp.add(new VentasP("Herramienta e Industrias", "25/02/2019 14:46:57 -0600", "Alejandro Reyes", "Escalera Tijera Plegable Reforz Dobl Escalumex 18 es trd-10", "1", "5070.00", "5070.00"));
        lista_ventasp.add(new VentasP("Autos", "24/02/2019 18:12:26 -0600", "Christian Ramírez", "Volskwagen Jetta 2.5 Trendline Triptonic AT", "1", "248000.00", "248000.00"));
        lista_ventasp.add(new VentasP("Libros", "20/02/2019 16:35:52 -0600", "Bruno Rosales", "Jaque al Psicoanalista + Libro Sorpresa", "1", "200.00", "200.00"));
        lista_ventasp.add(new VentasP("Tecnología", "13/02/2019 09:50:49 -0600", "Diego Herrera", "Motorola G6 Play, 4g Lte 5.7 13mp Lector de Huella + Funda", "1", "3400.00", "3400.00"));

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
