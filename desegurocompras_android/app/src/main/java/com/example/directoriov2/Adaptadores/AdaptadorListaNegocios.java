package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.directoriov2.Modelos.Negocio;
import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorListaNegocios extends RecyclerView.Adapter<AdaptadorListaNegocios.ViewHolderListaNegocios> implements Filterable {
    private ArrayList<Negocio> listaNegocios;
    private ArrayList<Negocio> listaNegociosfilter;
    private Context context;
    private final OnItemClickListener listener;


    public interface OnItemClickListener {
        void onItemClick(Negocio item);

    }

    public AdaptadorListaNegocios(Context context, ArrayList<Negocio> listaNegocios, OnItemClickListener listener) {
        this.listaNegocios = listaNegocios;
        this.context = context;
        this.listener = listener;
        this.listaNegociosfilter = listaNegocios;
    }

    @NonNull
    @Override
    public AdaptadorListaNegocios.ViewHolderListaNegocios onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        layout = R.layout.lista_negocios_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderListaNegocios(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorListaNegocios.ViewHolderListaNegocios holder, int position) {
        holder.bind(listaNegocios.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return listaNegocios.size();
    }

    public class ViewHolderListaNegocios extends RecyclerView.ViewHolder {
        TextView titulo;
        ImageView imagen;

        public ViewHolderListaNegocios(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            imagen = (ImageView) itemView.findViewById(R.id.imagen);
        }

        public void bind(final Negocio item, final OnItemClickListener listener) {

            titulo.setText(item.getTitulo());

            Picasso.get()
                    .load(item.getImagen())
                    .error(R.drawable.i_photo_load)
                    .into(imagen);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                String url = "http://desegurocompras.ytv.mx/imagenes/no-resultados.jpg";
                String nombre = "no se encontraron resultados";
                if (charString.isEmpty()) {
                    listaNegocios = listaNegociosfilter;
                } else {
                    ArrayList<Negocio> filteredList = new ArrayList<>();
                    for (Negocio row : listaNegociosfilter) {
                        if (row.getTitulo().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    if (filteredList.isEmpty()) {
                        Negocio vacio = new Negocio(nombre, url, "");
                        filteredList.add(vacio);
                    }
                    listaNegocios = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listaNegocios;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listaNegocios = (ArrayList<Negocio>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
