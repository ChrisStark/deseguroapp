package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.directoriov2.Modelos.PuntosA;
import com.example.directoriov2.R;
import java.util.ArrayList;

public class AdaptadorPuntosA extends RecyclerView.Adapter<AdaptadorPuntosA.ViewHolderPuntosA> {

    private Context context;
    private ArrayList<PuntosA> puntosa;

    public AdaptadorPuntosA (Context context, ArrayList<PuntosA> puntosa) {

        this.context = context;
        this.puntosa = puntosa;

    }

    @Override
    public AdaptadorPuntosA.ViewHolderPuntosA onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout =0;
        layout = R.layout.puntos_acumulados_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderPuntosA(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorPuntosA.ViewHolderPuntosA holder, int position) {

        holder.corte.setText(puntosa.get(position).getCorte());
        holder.detalles.setText(puntosa.get(position).getDetalles());

    }

    @Override
    public int getItemCount() {
        return puntosa.size();
    }

    public class ViewHolderPuntosA extends RecyclerView.ViewHolder {

        private TextView corte;
        private TextView detalles;

        public ViewHolderPuntosA(View itemView) {
            super(itemView);

            corte = itemView.findViewById(R.id.pa_corte);
            detalles =  itemView.findViewById(R.id.pa_detalles);

        }
    }

}
