package com.example.directoriov2.Adaptadores;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.directoriov2.Modelos.Paquetes;
import com.example.directoriov2.R;

import java.util.ArrayList;

public class AdaptadorPaquetes extends RecyclerView.Adapter<AdaptadorPaquetes.ViewHolderPaquetes> {

    private Context context;
    private ArrayList<Paquetes> l_paquetes;

    public AdaptadorPaquetes(Context context, ArrayList<Paquetes> l_paquetes) {

        this.context = context;
        this.l_paquetes = l_paquetes;

    }

    @Override
    public AdaptadorPaquetes.ViewHolderPaquetes onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = 0;
        layout = R.layout.paquetes_row_items;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, null, false);
        return new ViewHolderPaquetes(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderPaquetes holder, int position) {

        holder.nombrepaq.setText(l_paquetes.get(position).getNombrepaq());
        holder.puntospaq.setText(l_paquetes.get(position).getPuntospaq());
        holder.serviciospaq.setText(l_paquetes.get(position).getServiciospaq());
        holder.numservpaq.setText(l_paquetes.get(position).getNumservpaq());
        holder.preciopaq.setText(l_paquetes.get(position).getPreciopaq());

    }

    @Override
    public int getItemCount() {

        return l_paquetes.size();

    }

    public class ViewHolderPaquetes extends RecyclerView.ViewHolder {

        private TextView nombrepaq;
        private TextView puntospaq;
        private TextView serviciospaq;
        private TextView numservpaq;
        private TextView preciopaq;

        public ViewHolderPaquetes(View itemView) {

            super(itemView);
            nombrepaq = itemView.findViewById(R.id.paq_nombre);
            puntospaq = itemView.findViewById(R.id.paq_puntos);
            serviciospaq = itemView.findViewById(R.id.paq_servicios);
            numservpaq = itemView.findViewById(R.id.paq_numserv);
            preciopaq = itemView.findViewById(R.id.paq_precio);

        }
    }
}