package com.example.directoriov2.Clases;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.directoriov2.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;

public class DialogoNuevaPublicacion extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final int MY_PERMISSIONS_REQUEST = 100;
    private static final int DURATION = 250;
    private static final int PICK_IMAGE = 100;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    public static String TAG = "DiaglogoNuevaPublicacion";

    View view;
    LinearLayout LinearTogglePrecioDescuento;
    Switch mySwitch;
    TextInputEditText nombre;
    TextInputEditText precio;
    TextInputEditText precio_descuento;
    TextInputEditText cantidad;
    TextInputEditText marca;
    TextInputLayout float_categoria;
    TextInputLayout float_MiCategoria;
    TextInputLayout float_condicion;
    TextInputLayout float_nombre;
    TextInputLayout float_precio;
    TextInputLayout float_precio_descuento;
    TextInputLayout float_cantidad;
    TextInputLayout float_imagen;
    TextInputLayout float_marca;
    Button btnSeleccionarImg,btnGuardar;
    ImageView imagen;
    Spinner spinnerCategoria,spinnerMicategoria,spinnerCondicion;
    Uri imageUri;

    public DialogoNuevaPublicacion() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.EstiloFullScreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogo_nueva_publicacion, container, false);
        setToolbar();
        init();
        return view;
    }

    private void init() {
        float_categoria = view.findViewById(R.id.float_categorias);
        float_MiCategoria = view.findViewById(R.id.float_MiCategoria);
        float_condicion = view.findViewById(R.id.float_condicion);
        float_nombre = view.findViewById(R.id.float_nombreProducto);
        float_precio = view.findViewById(R.id.float_precio);
        float_precio_descuento = view.findViewById(R.id.float_precio_descuento);
        float_cantidad = view.findViewById(R.id.float_cantidad);
        float_imagen = view.findViewById(R.id.float_imagen);
        float_marca = view.findViewById(R.id.float_marca);

        precio = view.findViewById(R.id.precio);
        nombre = view.findViewById(R.id.nombreProducto);
        cantidad = view.findViewById(R.id.cantidad);
        precio_descuento = view.findViewById(R.id.precio_descuento);
        marca = view.findViewById(R.id.marca);

        btnSeleccionarImg = view.findViewById(R.id.btnSeleccionarImg);
        btnGuardar = view.findViewById(R.id.btnGuardar);
        imagen = view.findViewById(R.id.imagen);
        LinearTogglePrecioDescuento = view.findViewById(R.id.LinearTogglePrecioDescuento);
        mySwitch = view.findViewById(R.id.mySwitch);

        spinnerCategoria = view.findViewById(R.id.categoria);
        spinnerMicategoria = view.findViewById(R.id.Micategoria);
        spinnerCondicion = view.findViewById(R.id.condicion);
        llenarSpinners();


        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    togglePrecioDescuento();
                } else
                    togglePrecioDescuento();
            }
        });
        btnGuardar.setOnClickListener(this);
        btnSeleccionarImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGuardar:
                validarCampos();
                //enviar datos al server
                break;
            case R.id.btnSeleccionarImg:
                seleccionaImagen();
                break;
        }

    }

    private void llenarSpinners() {
        ArrayList<String> lista = new ArrayList<>();
        lista.add(0,"Categorias");
        lista.add("Ropa");
        lista.add("Tecnologia y Electronicos");
        lista.add("Servicios");
        lista.add("Comida");
        lista.add("Electronica");
        lista.add("Electronica");
        lista.add("Electronica");
        lista.add("Electronica");
        lista.add("Electronica");
        lista.add("Electronica");
        lista.add("Electronica");
        lista.add("Electronica");
        lista.add("Electronica");
        ArrayAdapter adaptadorCategoria;
        ArrayAdapter<String> adaptadorMiCategoria;
        ArrayAdapter<String> adaptadorCondicion;
        adaptadorCategoria = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,lista);
        adaptadorMiCategoria = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,lista);
        adaptadorCondicion = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,lista);
        spinnerCategoria.setAdapter(adaptadorCategoria);
        spinnerMicategoria.setAdapter(adaptadorMiCategoria);
        spinnerCondicion.setAdapter(adaptadorCondicion);
        spinnerCategoria.setPrompt("Elije una opcion");
        spinnerMicategoria.setPrompt("Elije una opcion");
        spinnerCondicion.setPrompt("Elije una opcion");
        spinnerCategoria.setOnItemSelectedListener(this);
        spinnerMicategoria.setOnItemSelectedListener(this);

    }
    public void validacion_Edittext(TextInputLayout float_text ,String text){
        if (!TextUtils.isEmpty(text)) {
            float_text.setError(null);
        }
        else{
            float_text.setError("Campo vacio");
        }
    }
    public void validacion_imagen(ImageView img){
        if (img.getDrawable() !=null){
            float_imagen.setError(null);
        }else{
            float_imagen.setError("seleccione una imagen");
        }

    }
    public void validacion_spinner(TextInputLayout float_text, String spinner){
        if (!spinner.equals("Categorias")){
            float_text.setError(null);
        }else{
            float_text.setError("Selecciona una opcion");
        }
    }

    public void validarCampos(){
        validacion_Edittext(float_nombre,nombre.getText().toString());
        validacion_Edittext(float_precio,precio.getText().toString());
        validacion_Edittext(float_precio_descuento,precio_descuento.getText().toString());
        validacion_Edittext(float_cantidad,cantidad.getText().toString());
        validacion_Edittext(float_marca,marca.getText().toString());
        validacion_imagen(imagen);
        validacion_spinner(float_categoria,spinnerCategoria.getSelectedItem().toString());
        validacion_spinner(float_MiCategoria,spinnerMicategoria.getSelectedItem().toString());
        validacion_spinner(float_condicion,spinnerCondicion.getSelectedItem().toString());

    }

    private void setToolbar() {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setTitle("Nueva Publicación");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
    public void togglePrecioDescuento() {
        if (LinearTogglePrecioDescuento.getVisibility() == View.GONE) {
            ExpandAndCollapseViewUtil.expand(LinearTogglePrecioDescuento, DURATION);

        } else {
            ExpandAndCollapseViewUtil.collapse(LinearTogglePrecioDescuento, DURATION);
        }
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        Dialog dialog = getDialog();
//        if (dialog != null) {
//            int width = ViewGroup.LayoutParams.MATCH_PARENT;
//            int height = ViewGroup.LayoutParams.MATCH_PARENT;
//            dialog.getWindow().setLayout(width, height);
//        }
//    }
    private void abrirGaleria() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    private void tomarFoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            Picasso.get().load(imageUri).fit().into(imagen);
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imagen.setImageBitmap(imageBitmap);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getItemAtPosition(position).equals("Categorias")){

        } else {
            String item = parent.getItemAtPosition(position).toString();
            Toast.makeText(getContext(), "selecciono " + item, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void cargarDialogoRecomendacion() {
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(getContext());
        alertOpciones.setTitle("Permisos Desactivados");
        alertOpciones.setMessage("Debe aceptar los permisos para el correcto funcionamiento de la App");
        alertOpciones.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS_REQUEST);
            }
        });

        alertOpciones.show();

    }


    private void seleccionaImagen() {

        final CharSequence[] opciones = {"Tomar Foto", "Cargar Imagen"};
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(getActivity());
        alertOpciones.setTitle("Seleccione una Opción");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("Tomar Foto")) {
                    tomarFoto();
                } else {
                    if (opciones[i].equals("Cargar Imagen")) {
                        abrirGaleria();
                    }
                }
            }
        });
        alertOpciones.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertOpciones.show();

    }


}
